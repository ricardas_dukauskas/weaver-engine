precision mediump float;

varying vec2 uvCoord;
uniform sampler2D texture0;

uniform vec2 resolution;

void main(void)
{
	float pixelWidth = 1.0 / resolution.x  * 5.0;
	float pixelHeight = 1.0 / resolution.y * 5.0;
	
	highp int x = int(uvCoord.x / pixelWidth);
	highp int y = int(uvCoord.y / pixelHeight);
	
	vec2 uv = vec2( float(x)* pixelWidth, float(y) * pixelHeight);	
	
	gl_FragColor = texture2D( texture0, uv );
}