precision mediump float;

varying vec2 uvCoord;

uniform sampler2D u_texture0;
uniform vec4 u_color;
uniform bool u_useTexture;

void main(void)
{
	vec4 color = vec4(1.0, 1.0, 1.0, 1.0);

	if (u_useTexture)
	{
		color = texture2D( u_texture0, uvCoord );
	}

	gl_FragColor = color * u_color;
}