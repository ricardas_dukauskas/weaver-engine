precision mediump float;

attribute vec3 a_vertex;

uniform mat4 u_MVMatrix;
uniform mat4 u_PMatrix;

void main(void)
{
	gl_Position = u_PMatrix * u_MVMatrix * vec4(a_vertex, 1.0);
}