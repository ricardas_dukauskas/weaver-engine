precision mediump float;

varying vec2 uvCoord;

uniform vec2 resolution;
uniform float time;

const float PI = 3.141592653;
const float NUM_ITERATIONS = 3.0;
const float DEFORM_SPEED = 800.0;
const float BRIGHTNESS = 3.0;
const float INV_SCALE = 30.0;

void main(void)
{
	vec2 v = uvCoord * resolution / INV_SCALE;
	v.x -= cos(time) * 0.5;
	v.y += sin(time) * 0.5;
	
	float color = 0.0;
	for(float i = 0.0; i < NUM_ITERATIONS; i++) 
	{
	  	float a = i * (PI/NUM_ITERATIONS);
		color += cos(PI * (v.y * cos(a) + v.x * sin(a) + sin(time * 0.001) * DEFORM_SPEED) );
	}
	
	color /= BRIGHTNESS;
	
	gl_FragColor = vec4(color, -color, -color*4.0, 1.0);
}