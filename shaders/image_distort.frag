precision mediump float;

varying vec2 uvCoord;
uniform sampler2D texture0;

uniform vec2 resolution;
uniform float time;
uniform float scale;

void main(void)
{
	gl_FragColor = texture2D( texture0, uvCoord + scale * vec2( sin(time + resolution.x*uvCoord.x), cos(time + resolution.y*uvCoord.y) ));
}