precision mediump float;

attribute vec3 a_vertex;
attribute vec2 a_uvCoord;

uniform mat4 u_MVMatrix;
uniform mat4 u_PMatrix;

varying vec2 uvCoord;

void main(void)
{
	gl_Position = u_PMatrix * u_MVMatrix * vec4(a_vertex, 1.0);

	uvCoord = a_uvCoord;
}