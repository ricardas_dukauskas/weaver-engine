precision mediump float;

uniform mat3 NMatrix;
uniform vec3 scaleFactor;

uniform bool useTexture;
uniform sampler2D texture0;
uniform vec4 uColor;

//light
uniform vec3 lightColor;
//uniform vec3 light_pos;
uniform vec3 light_ambient;
uniform float shininess;
uniform bool useLight;

varying vec2 uvCoord;
varying vec3 vNormal;
varying vec3 lightDir;
varying vec3 viewDir;
varying vec3 halfVector;

void main(void)
{
	//texturing
	vec4 color;
	if (useTexture)
		color = texture2D( texture0, uvCoord ) * uColor;
	else
		color = uColor;

	//lightning
	if (useLight) 
	{
		vec3 normalDir = normalize(NMatrix * vNormal);
		vec3 transformedNormal = normalize(normalDir * scaleFactor);
		
		//ambient light
		vec4 lightWeight = vec4(light_ambient,1.0);
		
		//diffused light
		float directionalLightWeighting = max(dot(transformedNormal, normalize(lightDir)), 0.0);
		lightWeight += directionalLightWeighting * vec4(lightColor, 1.0);
		
		//specular light
		//vec3 HV = normalize(halfVector);
		//float NdotHV = max(dot(N, halfVector), 0.0);
		float specular = pow(max(dot(normalDir, normalize(halfVector)),0.0), shininess );
		lightWeight += specular * vec4(lightColor, 1.0);

		lightWeight.a = 1.0;
		gl_FragColor = color * (lightWeight * vec4(lightColor,1.0));
	}
}