precision mediump float;

varying vec2 uvCoord;
uniform sampler2D texture0;

void main(void)
{
	//gl_FragColor = texture2D( texture0, uvCoord );
	gl_FragColor = texture2D( texture0, uvCoord ) * vec4(1.0, 0.0, 0.0, 1.0);
}