precision mediump float;

attribute vec3 a_vertex;

varying vec2 uvCoord;

void main(void)
{
	gl_Position = vec4(a_vertex, 1.0);
	uvCoord = ( a_vertex.xy + vec2(1,1) ) / 2.0;
}