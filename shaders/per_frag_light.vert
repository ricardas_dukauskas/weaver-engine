precision mediump float;

attribute vec3 vertexPos;
attribute vec3 normal;
attribute vec2 texCoord;

uniform mat4 MVMatrix;
uniform mat4 PMatrix;
uniform vec3 light_pos;

varying vec2 uvCoord;
varying vec3 vNormal;
varying vec3 lightDir;
varying vec3 viewDir;
varying vec3 halfVector;

void main(void)
{
	vNormal = normal;
	uvCoord = texCoord;
	
	vec4 mvPos = MVMatrix * vec4(vertexPos, 1.0);
	gl_Position = PMatrix * mvPos;
	
    lightDir = light_pos - mvPos.xyz;
    viewDir =-(mvPos.xyz);
	halfVector = normalize(light_pos + viewDir);
}