precision mediump float;

varying vec2 uvCoord;
uniform sampler2D u_texture;

void main(void)
{
	gl_FragColor = texture2D( u_texture, uvCoord );
}