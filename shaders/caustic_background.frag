precision mediump float;

varying vec2 uvCoord;
uniform sampler2D texture0;

uniform vec2 resolution;
uniform float time;
uniform float scale;

float CausticPattern(vec2 pos)
{
	return
	(
		sin(-20.0 	 * pos.x + time) 	  + 
		pow(sin(-3.0 * pos.x + time),5.0) + 
		pow(sin(20.0 * pos.x + time),3.0) + 
		pow(sin(13.0 * pos.x + time),2.0) + 
		pow(sin(30.0 * pos.x + time),2.0) + 
		pow(sin(90.0 * pos.x + time),2.0) 
	)/2.0;
}

vec2 DistortedCaustic(vec2 pos)
{
	pos.x *= (pos.y * 0.2) + 0.5;
	pos.x *= 1.0 + cos(time) * 0.1;
	return pos;
}

void main(void)
{
	vec2 pos = uvCoord;
	float strength;
	
	pos.x -= 0.5;
	
	vec2 distortedDomain = DistortedCaustic(pos);
	float causticPattern = CausticPattern(distortedDomain);
	float causticShape = clamp(70.0 - distortedDomain.x * 20.0, 0.0, 1.0);
	
	float caustic;
	caustic = causticPattern * causticShape;
	caustic *= (pos.y + 0.5) / 2.0;
	strength = length(pos + vec2(-0.5, 0.5)) * length(pos + vec2(0.2, 0.2)) * (caustic + 1.0);
	
	gl_FragColor = vec4( vec3(0.1, 0.3, 0.4) * strength, 1.0);
}