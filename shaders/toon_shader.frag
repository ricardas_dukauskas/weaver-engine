precision mediump float;

uniform mat3 NMatrix;
uniform vec3 scaleFactor;

uniform bool useTexture;
uniform sampler2D texture0;
uniform vec4 uColor;

//light
uniform vec3 lightColor;
uniform vec3 light_pos;
uniform vec3 light_ambient;
uniform float shininess;
uniform bool useLight;

varying vec2 uvCoord;
varying vec3 vNormal;
varying vec3 lightDir;
varying vec3 viewDir;
varying vec3 halfVector;

void main(void)
{
	vec3 normalDir = normalize(NMatrix * vNormal);
	vec4 lightWeight = vec4(1.0,1.0,1.0,1.0);
	
	//texturing
	vec4 color;
	if (useTexture)
		color = texture2D( texture0, uvCoord ) * uColor;
	else
		color = uColor;

	//outline
	float sil = max( dot( normalDir, normalize(viewDir) ), 0.0 );
	
	if (sil < 0.2)
	{
		gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
	}
	else
	{
		//lightning
		if (useLight) 
		{
			
			vec3 transformedNormal = normalize(normalDir * scaleFactor);
			
			//specular light
			//vec3 HV = normalize(halfVector);
			//float NdotHV = max(dot(N, halfVector), 0.0);
			float specular = pow(max(dot(normalDir, normalize(halfVector)),0.0), shininess );

			if (specular < 0.2) lightWeight *= 0.8;
			
			//diffused light
			float directionalLightWeighting = max(dot(transformedNormal, normalize(lightDir)), 0.0);
			if (directionalLightWeighting < 0.2) lightWeight *= 0.0;
			else if (directionalLightWeighting < 0.5) lightWeight *= 0.8; //
			
			//ambient light
			lightWeight += vec4(light_ambient,1.0);
			
			color.a = 1.0;
			lightWeight.a = 1.0;
			gl_FragColor = color * (lightWeight * vec4(lightColor,1.0));
		}
	}
}