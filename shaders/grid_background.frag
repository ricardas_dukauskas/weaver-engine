precision mediump float;

varying vec2 uvCoord;

uniform vec2 u_resolution;
uniform float u_time;
uniform float u_bigSize;
uniform float u_smallSize;
uniform float u_bigThickness;
uniform float u_smallThickness;


//const float PI_2 = 1.57079632679;
const float PI = 3.141592653;

float gridPattern(vec2 pos, float gridSize, float thickness)
{
	float str = 0.0;
	
	str += sin( clamp((mod(pos.x, gridSize) - (gridSize - 1.0) + thickness) / thickness, 0.0, 1.0) * PI );
	str += sin( clamp((mod(pos.y, gridSize) - (gridSize - 1.0) + thickness) / thickness, 0.0, 1.0) * PI );
	
	return str;
}

void main(void)
{
	float mainGrid;
	float smallGrid;

	mainGrid = gridPattern((uvCoord + u_time * 0.08) * u_resolution, u_bigSize, u_bigThickness);
	smallGrid = gridPattern(( vec2(uvCoord.x - u_time * 0.05, uvCoord.y + u_time * 0.04)) * u_resolution, u_smallSize, u_smallThickness);
	
	mainGrid = clamp(mainGrid, 0.0, 1.0);
	smallGrid = clamp(smallGrid, 0.0, 0.25);
	
	gl_FragColor = vec4( vec3(0.0, abs(mainGrid * (0.1 + sin(u_time))) + smallGrid * ( 0.5 * pow(cos(u_time * 2.0), 2.0)), smallGrid + abs(mainGrid ) ), 1.0);
}