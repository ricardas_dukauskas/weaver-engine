/**
 * Holds all the necesary data to render models to the screen. 
 */
function Graphics()
{
	//some additional buffers
	this.framebuffer;
	this.renderedTexture;
	this.renderBuffer;
	
	//holds a quad mesh (used for post processig)
	this.vertexQuad;
	
	//create matrices
	this.mvMatrix = mat4.create();
	this.pMatrix = mat4.create();
	this.lightMVMatrix  = mat4.create();
	this.biasMatrix;
	this.mvMatrixStack = [];
}

Graphics.prototype.Resize = function()
{
	//init frame buffer
	this.framebuffer = gl.createFramebuffer();
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffer);
	this.framebuffer.width = gl.viewportWidth;
	this.framebuffer.height = gl.viewportHeight;
	
	//create empty texture;
	this.renderedTexture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, this.renderedTexture);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this.framebuffer.width, this.framebuffer.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
	
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	gl.bindTexture(gl.TEXTURE_2D, null);
	
	//create empty depth buffer;
	this.renderBuffer = gl.createRenderbuffer();
    gl.bindRenderbuffer(gl.RENDERBUFFER, this.renderBuffer);
	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this.framebuffer.width, this.framebuffer.height);
	
	//attach components to the newly created buffers
	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.renderedTexture, 0);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this.renderBuffer);
	
	//bind default buffers
	gl.bindTexture(gl.TEXTURE_2D, null);
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);
	if (!this.RenderToTexture)
	{
	    this.RenderToScreen();
   	}
   	else
   	{
   		this.RenderToTexture();
   	}
};

Graphics.prototype.Initialize = function()
{
	//get some extentions
	gl.getExtension("WEBKIT_WEBGL_depth_texture"); 
	gl.getExtension("MOZ_WEBGL_depth_texture");
	
	// mat that divides by 2 and translates by 0.5 so -1, 1 would be 0, 1
	var biasMat = [0.5, 0, 0, 0.5, 0, 0.5, 0, 0.5, 0, 0, 0.5, 0, 0, 0, 0, 1];
	this.biasMatrix = mat4.create(biasMat);
	
	this.Resize();
    
    //load a full screen quad (multi-pass shading)
	this.vertexQuad = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexQuad);
	vertices = [
		 1.0,  1.0,  0.0,
		-1.0,  1.0,  0.0,
		 1.0, -1.0,  0.0,
		-1.0, -1.0,  0.0
	];
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
	this.vertexQuad.itemSize = 3;
	this.vertexQuad.numItems = 4;
	
};

/**
 * clears color and depth buffer 
 */
Graphics.prototype.glClear = function () {
	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
};

/**
 * pushes current model view matrix onto stack 
 */
Graphics.prototype.PushMatrix = function() {
	var copy = mat4.create();
	mat4.set(this.mvMatrix, copy);
	this.mvMatrixStack.push(copy);
};

/**
 * pops previous model view matrix from the stack
 * throws Ivalid Matrix if stack is empty 
 */
Graphics.prototype.PopMatrix = function() {
	if (this.mvMatrixStack.length == 0) 
	{
		throw "Invalid popMatrix!";
	}
	this.mvMatrix = this.mvMatrixStack.pop();
};

/**
 * sets current shader programs matrix uniforms
 * sets model view and projeciton matrix
 * also creates and sets normal matrix 
 */
Graphics.prototype.setMatrixUniforms = function() {
	gl.uniformMatrix4fv(currentShaderProgram.pMatrixUniform, false, pMatrix);
	gl.uniformMatrix4fv(currentShaderProgram.mvMatrixUniform, false, mvMatrix);
	
	var normalMatrix = mat3.create();
	mat4.toInverseMat3(mvMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
	gl.uniformMatrix3fv(currentShaderProgram.nMatrixUniform, false, normalMatrix);
};

/**
 * binds a texture, RGBA, linear filter, flipped Y
 * @param {Object} texure 
 */
Graphics.prototype.BindTexture = function (texture) {
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.bindTexture(gl.TEXTURE_2D, null);
};

/**
 * loads a texture from a path
 * @param {Object} path - path to the image
 * @return texture 
 */
Graphics.prototype.LoadTexture = function (path) {
	var texture;
	texture = gl.createTexture();
	texture.image = new Image();
	texture.image.onload = function () {
		Graphics.prototype.BindTexture(texture);
	};

	texture.image.src = path;
	
	return texture;
};

Graphics.prototype.RenderToTexture = function()
{
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffer);
};

Graphics.prototype.RenderToScreen = function()
{
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
};

Graphics.prototype.CreateText = function(text, font)
{
	// calculate texture size
	font.Activate();
	canvas2D.width = getPowerOfTwo(ctx.measureText(text).width);
	canvas2D.height = getPowerOfTwo(2*font.size);
	
	// draw texture
	ctx.fillStyle = font.backgroundColor;
	ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	
	font.Activate();
	ctx.fillStyle = font.color;
	
	var x = 0;
	
	switch(font.textAlign)
	{
		case 'left': 	x = 0; break; 
		case 'center': 	x = canvas2D.width/2; break;
		case 'right': 	x = canvas2D.width; break;
		default: 
	}
	
	ctx.fillText(text, x, canvas2D.height/2);
	
	// create texture
	var texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, canvas2D);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.bindTexture(gl.TEXTURE_2D, null);
	
	return texture;
};

Graphics.prototype.Draw = function(camera, cull, renderables)
{
	var onScreenObjects = [];
	
	camera.Update(cull);
	
	// update object transforms
	for (var i = 0; i < renderables.length; ++i)
	{
		// cull
		if (cull)
		{
			renderables[i].UpdateBoundingBoxes();
			if (camera.frustum.TestBox(renderables[i].mesh.boundingBox) === true)
			{
				onScreenObjects.push(renderables[i]);
			}
		}
		else
		{
			renderables[i].UpdateTransform();
		}
	}
	
	if (!cull)
	{
		onScreenObjects = renderables;
	}
	
	// render
	for (var i = 0; i < onScreenObjects.length; ++i)
	{
		onScreenObjects[i].effect.Prepare(camera, "");
		onScreenObjects[i].effect.Draw(onScreenObjects[i]);
	}
};