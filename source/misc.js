/**
 * A support object 2D vector holds 2 values .X and .Y
 * @param x
 * @param y
 */
function Vector2(x,y)
{
	this.X = x;
	this.Y = y;
}

function InnerProduct(a,b) {
	return (a[0] * b[0] + a[1] * b[1] + a[2] * b[2]);
}

function DistanceVec3(a , b)
{
	return vec3.length( vec3.subtract( vec3.create(b), a) );
}

function ProjectVector(vec, mat)
{
	var x,y,z,w;
	var ans = vec3.create();

	x = vec[0];
	y = vec[1];
	z = vec[2];

	w = 1 / (x * mat[3] + y * mat[7] + z * mat[11] + mat[15]);
	
	ans[0] = (x * mat[0] + y * mat[4] + z * mat[8]  + mat[12]) * w;
	ans[1] = (x * mat[1] + y * mat[5] + z * mat[9]  + mat[13]) * w;
	ans[2] = (x * mat[2] + y * mat[6] + z * mat[10] + mat[14]) * w;
	
	return ans;
}

/**
 * converts degrees into radiens
 * @param degrees 
 * @return radians 
 */
function degToRad(degrees) {
	return degrees * Math.PI / 180;
};

/**
 * returns sign of the varieble
 * @param {Object} x - number
 * @return sign of the object (-1, 0, 1)
 */
function sign(x) { 
	return x > 0 ? 1 : x < 0 ? -1 : 0; 
}

/**
 * returns smaller value
 * @param a
 * @param b
 */
function min(a,b) {
	return a > b ? b : a;
}

/**
 * returns bigger value
 * @param a
 * @param b
 */
function max(a,b) {
	return a > b ? a : b;
}

/**
 * Allows binding specific objects functions.
 * Basically it makes a function pointer
 * @param {Object} scope
 * @param {Object} fn
 */
function bind( scope, fn ) {
	return function () {
		fn.apply( scope, arguments );
	};
}

/**
 * Calculates closest biggest power of 2 to the number
 * @param {int} value 
 * @param {int} pow
 * @return pow - power of 2
 */
function getPowerOfTwo(value, pow) {
	var pow = pow || 1;
	while(pow<value) {
		pow *= 2;
	}
	return pow;
}

/**
 * Returns internet explorer version
 * if not IE then returns false
 */
function getInternetExplorerVersion()
{
	var rv = false; // Return value assumes failure.
	if (navigator.appName == 'Microsoft Internet Explorer')
	{
		var ua = navigator.userAgent;
		var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null)
		rv = parseFloat( RegExp.$1 );
	}
	return rv;
};

/**
 * returns HTML radio button value
 * @param {Object} name - radio group element name
 */
function getRadioValue(name) 
{
	var inputs = document.getElementsByName(name);
	for (var i = 0; i < inputs.length; i++) 
	{
		if (inputs[i].checked) 
		{
			return inputs[i].value;
		}
	}
}