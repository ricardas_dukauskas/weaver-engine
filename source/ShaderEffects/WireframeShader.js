/**
 * A wireframe shader
 * since WebGL currently does not support glPolygonMode the wireframe is fake and can appear not as expected 
 */
function WireframeShader()
{
	this.shaderProgram;
	
	this.viewMatrix;
	this.projectionMatrix;
	
	// attributes
	this.a_vertex;
	
	// uniforms locations
	this.u_modelview;
	this.u_projection;
	
	this.u_color;
}

/**
 * Loads shader and gets uniform locations
 */
WireframeShader.prototype.Load = function()
{
	//currently overriding
	vert = "shaders/wireframe.vert";
	frag = "shaders/wireframe.frag";
	
	//load per-pixel phong-blin shader
	var fragmentShader = getShader(gl, frag);
	var vertexShader = getShader(gl, vert);

	this.shaderProgram = gl.createProgram();
	gl.attachShader(this.shaderProgram, vertexShader);
	gl.attachShader(this.shaderProgram, fragmentShader);
	gl.linkProgram(this.shaderProgram);

	if (!gl.getProgramParameter(this.shaderProgram, gl.LINK_STATUS)) {
		alert("Could not initialise shaders");
	}
	
	// get uniform locations
	this.a_vertex		= gl.getAttribLocation(this.shaderProgram, "a_vertex");

	this.u_projection 	= gl.getUniformLocation(this.shaderProgram, "u_PMatrix");
	this.u_modelView	= gl.getUniformLocation(this.shaderProgram, "u_MVMatrix");

	this.u_color 		= gl.getUniformLocation(this.shaderProgram, "u_color");
	
};

/**
 * Just re/Initializes view, projection matrices and valid lights
 * @param {Obejet} view - view matrix
 * @param {Object} projection - projection matrix
 * @param {Object} light - a light object 
 */
WireframeShader.prototype.Prepare = function (camera)
{
	this.viewMatrix = camera.view;
	this.projectionMatrix = camera.projection;
};

/**
 * renders the object to the current frame buffer
 * @param {Object} object - a game object with an array of meshes
 */
WireframeShader.prototype.Draw = function(object) {
	gl.useProgram(this.shaderProgram);
	
	//move object to the correct position in the world 
	var mvMatrix = mat4.create(this.viewMatrix);
	mat4.multiply(mvMatrix, object.localMat, mvMatrix);
	// mat4.translate(mvMatrix, [object.position[0], object.position[1], object.position[2]]);
	// mat4.rotate(mvMatrix, degToRad(object.rotation[0]), [1, 0, 0]);
	// mat4.rotate(mvMatrix, degToRad(object.rotation[1]), [0, 1, 0]);
	// mat4.rotate(mvMatrix, degToRad(object.rotation[2]), [0, 0, 1]);
	// mat4.scale(mvMatrix, [object.scale[0], object.scale[1], object.scale[2]], mvMatrix);

	//get textures ready and draw	
	gl.enableVertexAttribArray(this.a_vertex);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, object.mesh.model.verticesPtr);
	gl.vertexAttribPointer(this.a_vertex, object.mesh.model.verticesPtr.itemSize, gl.FLOAT, false, 0, 0);

	//send uniforms
	gl.uniformMatrix4fv(this.u_projection, false, this.projectionMatrix);
	gl.uniformMatrix4fv(this.u_modelView, false, mvMatrix);

	gl.uniform4f(this.u_color, object.color[0], object.color[1], object.color[2], 1.0);
	
	//draw model
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, object.mesh.model.indicesPtr);
	gl.drawElements(gl.LINES, object.mesh.model.indicesPtr.numItems, gl.UNSIGNED_SHORT, 0);

	gl.disableVertexAttribArray(this.a_vertex);
};