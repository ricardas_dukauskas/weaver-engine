/**
 * Dynamic background effects
 * generates a cusom texture and renders it to the current frame buffer
 */
function GridBackgroundShader()
{
	this.shaderProgram;
	
	// attribute locations
	this.a_vertex;

	// uniform locations
	this.u_resolution;
	this.u_time;
	this.u_bigSize;
	this.u_smallSize;
	this.u_bigThickness;
	this.u_smallThickness;
	
	//default values
	this.bigSize = 120;
	this.smallSize = 50;
	this.bigThickness = 10;
	this.smallThickness = 3;
}

/**
 * Loads shader and gets uniform locations
 */
GridBackgroundShader.prototype.Load = function()
{
	vert = "shaders/image.vert";
	frag = "shaders/grid_background.frag";
	
	//load second pass shader, pixelate image
	var fragmentShader = getShader(gl, frag);
	var vertexShader = getShader(gl, vert);

	this.shaderProgram = gl.createProgram();
	gl.attachShader(this.shaderProgram, vertexShader);
	gl.attachShader(this.shaderProgram, fragmentShader);
	gl.linkProgram(this.shaderProgram);

	if (!gl.getProgramParameter(this.shaderProgram, gl.LINK_STATUS)) {
		alert("Could not initialise shaders");
	}

	this.a_vertex = gl.getAttribLocation(this.shaderProgram, "a_vertex");
	
	this.u_resolution = gl.getUniformLocation(this.shaderProgram, "u_resolution");
	this.u_time = gl.getUniformLocation(this.shaderProgram, "u_time");
	this.u_bigSize = gl.getUniformLocation(this.shaderProgram, "u_bigSize");
	this.u_smallSize = gl.getUniformLocation(this.shaderProgram, "u_smallSize");
	this.u_bigThickness = gl.getUniformLocation(this.shaderProgram, "u_bigThickness");
	this.u_smallThickness = gl.getUniformLocation(this.shaderProgram, "u_smallThickness");

};	

/**
 * Renders a shader generated Texture to the current frame buffer
 * @param {float} time - this is used of animation effect
 * note that it's not delta time but actual time passed in total
 */
GridBackgroundShader.prototype.Draw = function (time)
{
	gl.useProgram(this.shaderProgram);
	
	//bind texture
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, engine.Graphics.renderedTexture);

	gl.enableVertexAttribArray(this.a_vertex);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, engine.Graphics.vertexQuad);
	gl.vertexAttribPointer(this.a_vertex, engine.Graphics.vertexQuad.itemSize, gl.FLOAT, false, 0, 0);

	//send uniforms
	gl.uniform1f(this.u_bigSize, 		this.bigSize);
	gl.uniform1f(this.u_smallSize, 		this.smallSize);
	gl.uniform1f(this.u_bigThickness, 	this.bigThickness);
	gl.uniform1f(this.u_smallThickness, this.smallThickness);

	gl.uniform1f(this.u_time, time);
	gl.uniform2f(this.u_resolution, engine.Graphics.framebuffer.width, engine.Graphics.framebuffer.height); 
	//draw quad
	gl.drawArrays(gl.TRIANGLE_STRIP, 0, engine.Graphics.vertexQuad.numItems);
	
	gl.disableVertexAttribArray(this.a_vertex);
};