/**
 * Post processing effects
 * Renders unmodified texture to the screen 
 */
function RenderTextureShader()
{
	this.shaderProgram;
	
	this.a_vertex;
	this.u_texture;
}

/**
 * Loads shader and gets uniform locations
 */
RenderTextureShader.prototype.Load = function()
{
	vert = "shaders/image.vert";
	frag = "shaders/image_texture.frag";
	
	//load second pass shader, pixelate image
	var fragmentShader = getShader(gl, frag);
	var vertexShader = getShader(gl, vert);

	this.shaderProgram = gl.createProgram();
	gl.attachShader(this.shaderProgram, vertexShader);
	gl.attachShader(this.shaderProgram, fragmentShader);
	gl.linkProgram(this.shaderProgram);

	if (!gl.getProgramParameter(this.shaderProgram, gl.LINK_STATUS)) {
		alert("Could not initialise shaders");
	}

	this.a_vertex = gl.getAttribLocation(this.shaderProgram, "a_vertex");
	this.u_texture = gl.getUniformLocation(this.shaderProgram, "u_texture");
};	

/**
 * Renders the rendered Texture (back buffer) to the current frame buffer
 */
RenderTextureShader.prototype.Draw = function ()
{
	gl.useProgram(this.shaderProgram);
	
	//bind texture
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, engine.Graphics.renderedTexture);
	gl.uniform1i(this.u_texture, 0);
	
	gl.enableVertexAttribArray(this.a_vertex);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, engine.Graphics.vertexQuad);
	gl.vertexAttribPointer(this.a_vertex, engine.Graphics.vertexQuad.itemSize, gl.FLOAT, false, 0, 0);
	
	//draw quad
	gl.drawArrays(gl.TRIANGLE_STRIP, 0, engine.Graphics.vertexQuad.numItems);
	
	gl.disableVertexAttribArray(this.a_vertex);
};