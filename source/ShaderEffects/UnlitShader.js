/**
 * An unlit shader
 * supports texturing
 */
function UnlitShader()
{
	this.shaderProgram;
	
	this.viewMatrix;
	this.projectionMatrix;
	
	// attributes
	this.a_vertex;
	this.a_uvCoord;
	
	// uniforms locations
	this.u_modelview;
	this.u_projection;
	
	this.u_useTexture;
	this.u_texture;
	this.u_color;
}

/**
 * Loads shader and gets uniform locations
 */
UnlitShader.prototype.Load = function()
{
	//currently overriding
	vert = "shaders/no_light.vert";
	frag = "shaders/no_light.frag";
	
	//load per-pixel phong-blin shader
	var fragmentShader = getShader(gl, frag);
	var vertexShader = getShader(gl, vert);

	this.shaderProgram = gl.createProgram();
	gl.attachShader(this.shaderProgram, vertexShader);
	gl.attachShader(this.shaderProgram, fragmentShader);
	gl.linkProgram(this.shaderProgram);

	if (!gl.getProgramParameter(this.shaderProgram, gl.LINK_STATUS)) {
		alert("Could not initialise shaders");
	}
	
	// get uniform locations
	this.a_vertex		= gl.getAttribLocation(this.shaderProgram, "a_vertex");
	this.a_uvCoord 		= gl.getAttribLocation(this.shaderProgram, "a_uvCoord");

	this.u_projection 	= gl.getUniformLocation(this.shaderProgram, "u_PMatrix");
	this.u_modelView	= gl.getUniformLocation(this.shaderProgram, "u_MVMatrix");

	this.u_useTexture 	= gl.getUniformLocation(this.shaderProgram, "u_useTexture");
	this.u_texture 		= gl.getUniformLocation(this.shaderProgram, "u_texture0");
	this.u_color 		= gl.getUniformLocation(this.shaderProgram, "u_color");
	
};

/**
 * Just re/Initializes view, projection matrices and valid lights
 * @param {Obejet} view - view matrix
 * @param {Object} projection - projection matrix
 * @param {Object} light - a light object 
 */
UnlitShader.prototype.Prepare = function (camera)
{
	this.viewMatrix = camera.view;
	this.projectionMatrix = camera.projection;
};

/**
 * renders the object to the current frame buffer
 * @param {Object} object - a game object with an array of meshes
 */
UnlitShader.prototype.Draw = function(object) {
	gl.useProgram(this.shaderProgram);
	
	// gl.enable(gl.BLEND);
	// gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
	
	//move object to the correct position in the world 
	var mvMatrix = mat4.create(this.viewMatrix);
	mat4.multiply(mvMatrix, object.localMat, mvMatrix);
	// mat4.translate(mvMatrix, [object.position[0], object.position[1], object.position[2]]);
	// mat4.rotate(mvMatrix, degToRad(object.rotation[0]), [1, 0, 0]);
	// mat4.rotate(mvMatrix, degToRad(object.rotation[1]), [0, 1, 0]);
	// mat4.rotate(mvMatrix, degToRad(object.rotation[2]), [0, 0, 1]);
	// mat4.scale(mvMatrix, [object.scale[0], object.scale[1], object.scale[2]], mvMatrix);

	//get textures ready and draw
	var useTextures;
	useTextures = object.mesh.isTextured;
	gl.uniform1i(this.u_useTexture, useTextures);
	if (useTextures)
	{
		//bind texture
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, object.mesh.texture);
		gl.uniform1i(this.u_texture, 0);
	}

	gl.enableVertexAttribArray(this.a_vertex);
	gl.enableVertexAttribArray(this.a_uvCoord);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, object.mesh.model.verticesPtr);
	gl.vertexAttribPointer(this.a_vertex, object.mesh.model.verticesPtr.itemSize, gl.FLOAT, false, 0, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, object.mesh.model.vTexturePtr);
	gl.vertexAttribPointer(this.a_uvCoord, object.mesh.model.vTexturePtr.itemSize, gl.FLOAT, false, 0, 0);
	
	//send uniforms
	gl.uniformMatrix4fv(this.u_projection, false, this.projectionMatrix);
	gl.uniformMatrix4fv(this.u_modelView, false, mvMatrix);

	gl.uniform4f(this.u_color, object.color[0], object.color[1], object.color[2], 1.0);
	
	//draw model
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, object.mesh.model.indicesPtr);
	gl.drawElements(gl.TRIANGLES, object.mesh.model.indicesPtr.numItems, gl.UNSIGNED_SHORT, 0);
	
	gl.disableVertexAttribArray(this.a_vertex);
	gl.disableVertexAttribArray(this.a_uvCoord);
	
	// gl.disable(gl.BLEND);
};