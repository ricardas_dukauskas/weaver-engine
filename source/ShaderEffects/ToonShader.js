/**
 * A toon shader uses per fragment light
 * supports texturing and 1 light source
 */
function ToonShader()
{
	this.shaderProgram;
	
	this.viewMatrix;
	this.projectionMatrix;
	this.light;
	
	// attributes
	this.a_vertex;
	this.a_normal;
	this.a_uvCoord;
	
	// uniforms locations
	this.u_modelview;
	this.u_projection;
	this.u_normalMatrix;
	
	this.u_useTexture;
	this.u_texture;
	this.u_color;
	this.u_scale;
	this.u_shininess;
	
	this.u_useLight;
	this.u_ambient;
	this.u_lightPos;
	this.u_lightColor;
}

/**
 * Loads shader and gets uniform locations
 */
ToonShader.prototype.Load = function()
{
	//currently overriding
	vert = "shaders/toon_shader.vert";
	frag = "shaders/toon_shader.frag";
	
	//load per-pixel phong-blin shader
	var fragmentShader = getShader(gl, frag);
	var vertexShader = getShader(gl, vert);

	this.shaderProgram = gl.createProgram();
	gl.attachShader(this.shaderProgram, vertexShader);
	gl.attachShader(this.shaderProgram, fragmentShader);
	gl.linkProgram(this.shaderProgram);

	if (!gl.getProgramParameter(this.shaderProgram, gl.LINK_STATUS)) {
		alert("Could not initialise shaders");
	}
	
	// get uniform locations
	this.a_vertex		= gl.getAttribLocation(this.shaderProgram, "vertexPos");
	this.a_normal 		= gl.getAttribLocation(this.shaderProgram, "normal");
	this.a_uvCoord 		= gl.getAttribLocation(this.shaderProgram, "texCoord");

	this.u_projection 	= gl.getUniformLocation(this.shaderProgram, "PMatrix");
	this.u_modelView	= gl.getUniformLocation(this.shaderProgram, "MVMatrix");
	this.u_normalMatrix = gl.getUniformLocation(this.shaderProgram, "NMatrix");
	this.u_scale 		= gl.getUniformLocation(this.shaderProgram, "scaleFactor");
	
	this.u_lightColor 	= gl.getUniformLocation(this.shaderProgram, "lightColor");
	this.u_ambient 		= gl.getUniformLocation(this.shaderProgram, "light_ambient");
	this.u_lightPos 	= gl.getUniformLocation(this.shaderProgram, "light_pos");
	this.u_shininess 	= gl.getUniformLocation(this.shaderProgram, "shininess");
	this.u_useLight 	= gl.getUniformLocation(this.shaderProgram, "useLight");

	this.u_useTexture 	= gl.getUniformLocation(this.shaderProgram, "useTexture");
	this.u_texture 		= gl.getUniformLocation(this.shaderProgram, "texture0");
	this.u_color 		= gl.getUniformLocation(this.shaderProgram, "uColor");
	
};

/**
 * Just re/Initializes view, projection matrices and valid lights
 * @param {Obejet} view - view matrix
 * @param {Object} projection - projection matrix
 * @param {Object} light - a light object 
 */
ToonShader.prototype.Prepare = function (camera, light)
{
	this.viewMatrix = camera.view;
	this.projectionMatrix = camera.projection;
	this.light = light;
};

/**
 * renders the object to the current frame buffer
 * @param {Object} object - a game object with an array of meshes
 */
ToonShader.prototype.Draw = function(object) {
	gl.useProgram(this.shaderProgram);

	//add light 
	fixedLight = true;
	var lighting = true;//object.useLight;
	gl.uniform1i(this.u_useLight, lighting);
	if (lighting) {
		var lightpos = new glMatrixArrayType(4);
		
		if (fixedLight) {
			lightpos[0] = 0;
			lightpos[1] = 20;
			lightpos[2] = 0;
			lightpos[3] = 1;
			
			mat4.multiplyVec4(this.viewMatrix, lightpos, lightpos);
		} else {
			lightpos[0] = 0;
			lightpos[1] = 0;
			lightpos[2] = -5;
			lightpos[3] = 1;
		}
		
		gl.uniform3fv(this.u_ambient, [0.1, 0.1, 0.1]);
		gl.uniform3f(this.u_lightPos, lightpos[0], lightpos[1], lightpos[2]);
		gl.uniform3f(this.u_lightColor, 0.9, 0.9, 1.0);
		gl.uniform1f(this.u_shininess, 100);
	}
	
	//move object to the correct position in the world 
	var mvMatrix = mat4.create(this.viewMatrix);
	mat4.multiply(mvMatrix, object.localMat, mvMatrix);
	// mat4.translate(mvMatrix, [object.position[0], object.position[1], object.position[2]]);
	// mat4.rotate(mvMatrix, degToRad(object.rotation[0]), [1, 0, 0]);
	// mat4.rotate(mvMatrix, degToRad(object.rotation[1]), [0, 1, 0]);
	// mat4.rotate(mvMatrix, degToRad(object.rotation[2]), [0, 0, 1]);
	// mat4.scale(mvMatrix, [object.scale[0], object.scale[1], object.scale[2]], mvMatrix);

	//get textures ready and draw
	var useTextures;

	useTextures = object.mesh.isTextured;
	gl.uniform1i(this.u_useTexture, useTextures);
	if (useTextures)
	{
		//bind texture
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, object.mesh.texture);
		gl.uniform1i(this.u_texture, 0);
	}

	gl.enableVertexAttribArray(this.a_vertex);
	gl.enableVertexAttribArray(this.a_normal);
	gl.enableVertexAttribArray(this.a_uvCoord);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, object.mesh.model.verticesPtr);
	gl.vertexAttribPointer(this.a_vertex, object.mesh.model.verticesPtr.itemSize, gl.FLOAT, false, 0, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, object.mesh.model.normalsPtr);
	gl.vertexAttribPointer(this.a_normal, object.mesh.model.normalsPtr.itemSize, gl.FLOAT, false, 0, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, object.mesh.model.vTexturePtr);
	gl.vertexAttribPointer(this.a_uvCoord, object.mesh.model.vTexturePtr.itemSize, gl.FLOAT, false, 0, 0);
	
	//send uniforms
	gl.uniformMatrix4fv(this.u_projection, false, this.projectionMatrix);
	gl.uniformMatrix4fv(this.u_modelView, false, mvMatrix);
	
	//normal matrix
	var normalMatrix = mat3.create();
	mat4.toInverseMat3(mvMatrix, normalMatrix);
	mat3.transpose(normalMatrix);
	gl.uniformMatrix3fv(this.u_normalMatrix, false, normalMatrix);

	gl.uniform4f(this.u_color, object.color[0], object.color[1], object.color[2], 1.0);
	gl.uniform3fv(this.u_scale, object.scale);
	
	//draw model
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, object.mesh.model.indicesPtr);
	gl.drawElements(gl.TRIANGLES, object.mesh.model.indicesPtr.numItems, gl.UNSIGNED_SHORT, 0);
	
	gl.disableVertexAttribArray(this.a_vertex);
	gl.disableVertexAttribArray(this.a_normal);
	gl.disableVertexAttribArray(this.a_uvCoord);
};