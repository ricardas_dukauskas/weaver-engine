/**
 * Post processing effects
 * Applies pixelation effect
 */
function PixelateShader()
{
	this.shaderProgram;
	
	this.a_vertex;

	this.u_texture;
	this.u_resolution;
}

/**
 * Loads shader and gets uniform locations
 */
PixelateShader.prototype.Load = function()
{
	vert = "shaders/image.vert";
	frag = "shaders/image_pixelate.frag";
	
	//load second pass shader, pixelate image
	var fragmentShader = getShader(gl, frag);
	var vertexShader = getShader(gl, vert);

	this.shaderProgram = gl.createProgram();
	gl.attachShader(this.shaderProgram, vertexShader);
	gl.attachShader(this.shaderProgram, fragmentShader);
	gl.linkProgram(this.shaderProgram);

	if (!gl.getProgramParameter(this.shaderProgram, gl.LINK_STATUS)) {
		alert("Could not initialise shaders");
	}

	this.a_vertex = gl.getAttribLocation(this.shaderProgram, "a_vertex");
	
	this.u_texture = gl.getUniformLocation(this.shaderProgram, "texture0");
	this.u_resolution = gl.getUniformLocation(this.shaderProgram, "resolution");
};	

/**
 * Renders the rendered pixelated Texture to the current frame buffer
 */
PixelateShader.prototype.Draw = function ()
{
	gl.useProgram(this.shaderProgram);
	
	//bind texture
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, engine.Graphics.renderedTexture);
	gl.uniform1i(this.u_texture, 0);
	
	gl.enableVertexAttribArray(this.a_vertex);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, engine.Graphics.vertexQuad);
	gl.vertexAttribPointer(this.a_vertex, engine.Graphics.vertexQuad.itemSize, gl.FLOAT, false, 0, 0);

	//send uniforms
	gl.uniform2f(this.u_resolution, engine.Graphics.framebuffer.width, engine.Graphics.framebuffer.height); 
	
	//draw quad
	gl.drawArrays(gl.TRIANGLE_STRIP, 0, engine.Graphics.vertexQuad.numItems);
	
	gl.disableVertexAttribArray(this.a_vertex);
};