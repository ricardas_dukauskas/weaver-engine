/**
 * This object holds canvas parameters
 * @param {Object} canvas - HTML5 canvas
 * @param {Object} gl - webGL context
 */
function Settings(canvas, gl)
{
	this.gl = gl;
	this.canvas = canvas;
	this.Width = canvas.width;
	this.Height = canvas.height;
	this.Fullscreen = false;
	this.CorsorLocked = false;
	this.ViewportChanged = false;
	
	//register event listeners
	document.addEventListener('fullscreenchange', bind(this, this.FullscreenChange), false);
	document.addEventListener('mozfullscreenchange', bind(this, this.FullscreenChange), false);
	document.addEventListener('webkitfullscreenchange', bind(this, this.FullscreenChange), false);
}

/**
 * Resets some delta variebles
 */
Settings.prototype.Restore = function()
{
	//this.ViewportChanged = false;
};

/**
 * Toggles canvas window to fullscreen
 */
Settings.prototype.ToggleFullscreen = function () {
	if (!document.fullscreenElement &&    // alternative standard method
	!document.mozFullScreenElement && !document.webkitFullscreenElement) 
	{  // current working methods
		if (this.canvas.requestFullscreen) 
		{
			this.canvas.requestFullscreen();
		} 
		else if (this.canvas.mozRequestFullScreen) 
		{
			this.canvas.mozRequestFullScreen();
		} 
		else if (this.canvas.webkitRequestFullscreen) 
		{
			this.canvas.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		}
		
		this.Fullscreen = true;
	} 
	else 
	{
		if (document.cancelFullScreen) 
		{
			document.cancelFullScreen();
		} 
		else if (document.mozCancelFullScreen) 
		{
			document.mozCancelFullScreen();
		} 
		else if (document.webkitCancelFullScreen) 
		{
			document.webkitCancelFullScreen();
		}
		
		this.Fullscreen = false;
	}
};

/**
 * Called on full screen change.
 * If fullscreen - canvas and viewport size is set to screen size
 * If window - canvas and viewpoer size is set to initial size
 * Sets flag ViewportChanged to true, programmer is responsible to revert it back if necessary
 */
Settings.prototype.FullscreenChange = function () {
	if (document.webkitFullscreenElement === this.canvas ||
		document.mozFullscreenElement === this.canvas ||
		document.mozFullScreenElement === this.canvas) 
	{ 	
		// Older API upper case 'S'.
		// Element is fullscreen, now we can request pointer lock
		//engine.Input.LockPointer();
		
		this.canvas.width = screen.width;
		this.canvas.height = screen.height;
	}
	else
	{
		this.canvas.width = this.Width;
		this.canvas.height = this.Height;
	}
	
	this.gl.viewportWidth = this.canvas.width;
	this.gl.viewportHeight = this.canvas.height;
	
	this.ViewportChanged = true;
	
	engine.Graphics.Resize();
};

