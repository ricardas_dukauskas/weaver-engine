function Frustum()
{	
	this.planes = [];
	
	for (i = 0; i < 6; ++i)
	{
		var plane = new Plane();
		this.planes.push(plane);
	}
}

Frustum.prototype.Build = function(camera)
{
	var mvp = mat4.create();
	var mv = mat4.create(camera.view);
	var p = mat4.create(camera.projection);
	mat4.transpose(mv);
	mat4.transpose(p);
	mat4.multiply(mv, p, mvp);
	mat4.transpose(mvp);
	
	var a,b,c,d;
	// left 
	a = mvp[3] 	+ mvp[0];
	b = mvp[7] 	+ mvp[4];
	c = mvp[11] + mvp[8];
	d = mvp[15] + mvp[12];
	this.planes[0].Set(a,b,c,d); // left
	
	// right 
	a = mvp[3] 	- mvp[0];  
	b = mvp[7] 	- mvp[4];  
	c = mvp[11] - mvp[8];  
	d = mvp[15] - mvp[12]; 
	this.planes[1].Set(a,b,c,d); // right
	
	// bottom 
	a = mvp[3] 	+ mvp[1];  
	b = mvp[7] 	+ mvp[5];  
	c = mvp[11] + mvp[9];  
	d = mvp[15] + mvp[13]; 
	this.planes[2].Set(a,b,c,d); // bottom
	
	// top 
	a = mvp[3] 	- mvp[1];  
	b = mvp[7] 	- mvp[5];  
	c = mvp[11] - mvp[9];  
	d = mvp[15] - mvp[13]; 
	this.planes[3].Set(a,b,c,d); // top
	
	// near 
	a = mvp[3] 	+ mvp[2]; 
	b = mvp[7] 	+ mvp[6]; 
	c = mvp[11] + mvp[10];
	d = mvp[15] + mvp[14];
	this.planes[4].Set(a,b,c,d); // near
	
	// far
	a = mvp[3] 	- mvp[2];  
	b = mvp[7] 	- mvp[6];  
	c = mvp[11] - mvp[10]; 
	d = mvp[15] - mvp[14]; 
	this.planes[5].Set(a,b,c,d); // far
};

Frustum.prototype.TestPoint = function(vert)
{
	for (p = 0; p < 6; p++ )
	{
		if (this.planes[p].Distance(vert) < 0)
		{
			return false;
		}
	}
	return true;
};

Frustum.prototype.TestSphere = function(vert, radius)
{
	for (p = 0; p < 6; p++ )
	{
		if (this.planes[p].Distance(vert) <= -radius)
		{
			return false;
		}
	}
	return true;
};

Frustum.prototype.TestBox = function(box)
{
	var inside, outside;
	var result = true;
	var vertex = vec3.create();
	for (i = 0; i < 6; ++i)
	{
		inside = 0;
		outside = 0;
		for (k = 0; k < 8 && (inside == 0 || outside == 0); ++k)
		{
			if (this.planes[i].Distance(box.vertices[k]) < 0)
			{
				++outside;
			}
			else
			{
				++inside;
			}
		}
		if (inside == 0)
		{
			return false;
		}
		else if (outside > 0)
		{
			return true; // intersect
		}
	}
	
	return result;
};

/**
 * A camera object
 * has position rotation and projection matrix
 * generates view matrix on request 
 */
function Camera() {
	this.position = vec3.create();
	this.rotation = vec3.create();
	this.view = mat4.create();
	this.projection = mat4.create();
	this.combined = mat4.create();
	this.frustum = new Frustum();
	
	this.up = vec3.create();
	this.right = vec3.create();
	
	this.near;
	this.far;
	this.fov;
	this.aspectRatio;
	this.isOrtho;
}

Camera.prototype.PI 	 = 3.141592653;
Camera.prototype.TO_RADS = 0.017453293; // PI/180
Camera.prototype.TO_DEGR = 57.2957795130; // 180/PI

/**
 * sets camera rotaion
 * @param {float} x - yaw
 * @param {float} y - pitch
 * @param {float} z - roll
 * roll is not implemented
 */
Camera.prototype.setRotation = function(x, y, z) {
	this.rotation[0] = x;
	this.rotation[1] = y;
	this.rotation[2] = z;
};

/**
 * rotates camera 
 * @param {float} x - yaw
 * @param {float} y - pitch
 * @param {float} z - roll
 */
Camera.prototype.rotate = function(x, y, z) {
	this.rotation[0] += x;
	this.rotation[1] += y;
	this.rotation[2] += z;
};

/**
 * sets camera position
 * @param {float} x
 * @param {float} y
 * @param {float} z 
 */
Camera.prototype.setPosition = function(x, y, z) {
	this.position[0] = x;
	this.position[1] = y;
	this.position[2] = z;
};

/**
 * translates camera
 * @param {float} x
 * @param {float} y
 * @param {float} z 
 */
Camera.prototype.translate = function(x, y, z) {
	this.position[0] += x;
	this.position[1] += y;
	this.position[2] += z;
};


/**
 * moves camera forwards based on local axis
 * @param {float} units 
 */
Camera.prototype.moveForward = function(units) {
	var direction = this.CalculateDirection(this.rotation[0], this.rotation[1]);
	vec3.scale(direction, units, direction);
	vec3.add(this.position, direction, this.position);
};

/**
 * moves camera right based on local axis
 * @param {float} units 
 */
Camera.prototype.moveRight = function(units) {
	var direction = this.CalculateDirection(this.rotation[0]+this.PI/2, 0);
	vec3.scale(direction, units, direction);
	vec3.add(this.position, direction, this.position);
};

/**
 * moves camera up based on local axis
 * @param {float} units 
 */
Camera.prototype.moveUp = function(units) {
	//var direction = this.CalculateDirection(this.rotation[0], this.rotation[1] + this.PI/2);
	//vec3.scale(direction, units, direction);
	//vec3.add(position, direction, position);
	
	this.position[1] += units;
};

/**
 * {private} calculates look at direction
 * @return target
 */
Camera.prototype.GetDirection = function() {
	var direction = vec3.create;
	direction = this.CalculateDirection(this.rotation[0], this.rotation[1]);
	vec3.add(this.position, direction, direction);
	return direction;
};

/**
 * {private} calculates up vecotr
 */
Camera.prototype.GetUp = function() {
	return this.CalculateDirection(this.rotation[0], this.rotation[1]+this.PI/2);
};

/**
 * {private} calculates forward direction
 * @return {vec3} direction
 */
Camera.prototype.CalculateDirection = function(yaw, pitch) {
	var direction = vec3.create();
	direction[0] = Math.cos ( pitch ) * Math.sin ( yaw );
	direction[1] = Math.sin ( pitch );
	direction[2] -= Math.cos ( pitch ) * Math.cos ( yaw );
	return direction;
};

/**
 * updates and returns view matrix
 * @return {mat4} matrix 
 */
Camera.prototype.Update = function(rebuildFrustum) {
	//var up = this.GetUp();
	var direction = this.CalculateDirection(this.rotation[0], this.rotation[1]);
	var target = vec3.create();
	vec3.add(this.position, direction, target);
	// var right = vec3.create();
	// var up = vec3.create();
	var globalUp = vec3.create(); globalUp[1] = 1;
	
	//vec3.normalize(target,target);
	vec3.cross(globalUp,direction,this.right);
	vec3.cross(direction,this.right,this.up);
	
	mat4.lookAt(this.position, target, this.up, this.view);
	
	mat4.multiply(this.projection, this.view, this.combined);
	
	if (rebuildFrustum)
	{
		this.frustum.Build(this);
	}
	
	//return this.view;
};

/**
 * creates and stores perspective matrix 
 */
Camera.prototype.CreatePerspectiveProjection = function(fov, aspectRatio, near, far)
{
	this.near = near;
	this.far = far;
	this.fov = fov;
	this.aspectRatio = aspectRatio;
	this.isOrtho = false;
	mat4.perspective(fov, aspectRatio, near, far, this.projection);
};

/**
 * creates and stores orthographic matrix 
 */
Camera.prototype.CreateOrthographicProjection = function(left,right,top,bottom,near,far)
{
	this.near = near;
	this.far = far;
	this.fov = 0;
	this.aspectRatio = (right-left) / (bottom - top);
	this.isOrtho = true;
	mat4.ortho(left, right, top, bottom, near, far, this.projection);
};

/**
 * {private} calculates lookAt
 * kinda useless now but i will leave it for now 
 */
Camera.prototype.lookAt = function(/*eyes*/x,y,z, /*target*/ i,j,k, /*up*/ upx,upy,upz) {
	var up = vec3.create();
	up[0] = upx;
	up[1] = upy;
	up[2] = upz;
	
	var eyes = vec3.create();
	eyes[0] = x;
	eyes[1] = y;
	eyes[2] = z;
	
	var target = vec3.create();
	target[0] = i;
	target[1] = j;
	target[2] = k;
	
	mat4.lookAt(eyes, target, up, this.view);
};

Camera.prototype.Unproject = function(vec)
{
	var x = vec[0];
	var y = vec[1];
    y = gl.viewportHeight - y;
    vec[0] = (2 * x) / gl.viewportWidth - 1;
    vec[1] = (2 * y) / gl.viewportHeight - 1;
    vec[2] = 2 * vec[2] - 1;
    
    var invViewProj = mat4.create();
    mat4.inverse(this.combined, invViewProj);

    vec = ProjectVector(vec, invViewProj);
    return vec;
};

Camera.prototype.Project = function(vec)
{
	vec = ProjectVector(vec, this.combined);
    vec[0] = gl.viewportWidth * (vec[0] + 1) / 2;
    vec[1] = gl.viewportHeight * (vec[1] + 1) / 2;
    vec[2] = (vec[2] + 1) / 2;
};
