/**
 * loads GLSL shader source from file and compiles it
 * if file extention is .vert it will treat it as a vertex shader
 * if file extention is .frag it will treat it as a fragment shader
 * @param {Object} gl
 * @param {Object} path
 * @return {Shader} webgl ready shader
 */
function getShader(gl, path) {
	var file = null;
	var string;
	var stringLines;
	
	//get http data
	file = new XMLHttpRequest();
	file.open( "POST", path, false );
	file.send( null );
	
	//get raw data
	string = file.responseText;

	//get shader type by extention
	var ext = path.split(".");
	ext = ext[ext.length-1];
	
	var shader;
	if (ext == "frag") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (ext == "vert") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		return null;
	}

	gl.shaderSource(shader, string);
	gl.compileShader(shader);

	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return null;
	}

	return shader;
}