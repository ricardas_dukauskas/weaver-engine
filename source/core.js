//create webgl canvas
var gl;
var ctx;
var engine;

/**
 * Game engine object, holds all the main engine objects 
 * @param {Object} gl
 * @param {Object} canvas
 * @param {Object} ctx - context 2d
 */
function WeaverEngine(gl, canvas, ctx)
{
	this.gl = gl;
	this.canvas = canvas;
	this.ctx = ctx;
	this.Settings = new Settings(canvas, gl);
	this.Input = new InputHandler(canvas);
	this.Graphics = new Graphics();
	this.Audio = new AudioHandler();
	
	//detect IE
	this.IE = getInternetExplorerVersion();
	
	// reset some variebles that some browses do not handle
	this.Restore = function()
	{
		this.Settings.Restore();
		this.Input.Restore();
		this.Audio.Clean();
	};
}

function initGL(canvas, canvas2D) {
	try {
		gl = canvas.getContext("experimental-webgl");
		ctx = canvas2D.getContext("2d");
		gl.viewportWidth = canvas.width;
		gl.viewportHeight = canvas.height;
		canvas2D.style.display = 'none'; // hide the extra canvas
	} catch (e) {
	}
	if (!gl) {
		alert("Could not initialise WebGL, sorry but your browser can't even handle me right now. :)");
	}
}

//start webgl
function webGLStart() {
	//initialize webGL
	canvas = document.getElementById("canvas");
	canvas2D = document.getElementById("canvas2D");
	
	initGL(canvas,canvas2D);
	
	engine = new WeaverEngine(gl, canvas, ctx);
	
	engine.Graphics.Initialize();
	
	//load stuff
	initialize();

	//clear screen
	gl.clearColor(0.2, 0.2, 0.7, 1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	gl.cullFace(gl.BACK);

	//enter main loop
	tick();
}

//main loop
function tick() {
	requestAnimFrame(tick);
	update();
	engine.Restore();
}
