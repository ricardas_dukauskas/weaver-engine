////////////////////////////
// Plane
////////////////////////////

function Plane()
{
	this.normal = vec3.create();
	this.dist;
}

Plane.prototype.Set = function(a,b,c,d)
{
	var t = Math.sqrt(a * a + b * b + c * c);
	this.normal[0] = a / t ;
	this.normal[1] = b / t;
	this.normal[2] = c / t;
	this.dist = d / t;
};

Plane.prototype.Distance = function(p) {
	return (this.dist + InnerProduct(this.normal, p));
};

////////////////////////////
// Ray
////////////////////////////

function RayHit()
{
	this.distance = Infinity;
	this.object;
}

function Ray()
{
	this.origin;
	this.point;
	this.direction;
}

Ray.prototype.Create = function(start, end)
{
	this.origin = vec3.create(start);
	this.point = vec3.create(end);
	this.direction = vec3.create();
	vec3.subtract(end, start, this.direction);
	vec3.normalize(this.direction, this.direction);
};

Ray.prototype.PickRay = function(camera, objects)
{
	// create ray
	var start = vec3.create();
	start[0] = engine.Input.MousePosition.X;
	start[1] = engine.Input.MousePosition.Y;
	start[2] = 0;
	start = camera.Unproject(start);
	
	var end = vec3.create();
	end[0] = engine.Input.MousePosition.X;
	end[1] = engine.Input.MousePosition.Y;
	end[2] = 1;
	end = camera.Unproject(end);
	
	this.Create(start, end);
	
	// find shortest hit distance
	var hit = new RayHit();
	var dist = 0;
	for (var i = 0; i < objects.length; ++i)
	{
		objects[i].UpdateBoundingBoxes();
		dist = this.IntersectsBox(objects[i].mesh.boundingBox);
		
		if (dist >= 0 && dist < hit.distance)
		{
			hit.distance = dist;
			hit.object = objects[i];
		}
	}
	
	return hit;
};

Ray.prototype.DistanceToPoint = function(point)
{
    var direction = vec3.create();
    var directionDistance;
	vec3.subtract(point, this.origin, direction);
    vec3.normalize(direction, direction);
    directionAngle = vec3.dot(direction, this.direction);
	
	var dist = DistanceVec3(point, this.origin);
   
    // point behind the ray
    if ( directionAngle < 0 ) {
		return dist;
    }
	
	dist *= directionAngle;
	
	var v1 = vec3.create(this.direction);
    vec3.scale(v1, dist, v1);
    vec3.add( this.origin, v1, v1 );
    
    return DistanceVec3(v1, point);
};

Ray.prototype.IntersectsSphere = function(point, radius)
{
	return (this.DistanceToPoint(point) <= radius);
	
};

/**
* points are set clocwise
* builds plane and returns intersect distence -1 is not intersecting
* points are vec3 objects
*/
Ray.prototype.IntersectsPlane = function(P1, P2, P3, P4)
{
	var v1 = vec3.create(); // P2 - P1
	var v2 = vec3.create(); // P4 - P1
	var v3 = vec3.create(); // P4 - P3
	var v4 = vec3.create(); // I - P1
	var v5 = vec3.create(); // I - P3
	
	var intersect = vec3.create(); // I
	
	var plane = new Plane();
	var normal = vec3.create();
	var distance;
	
	// get parpendicular vectors for normal calculation
	vec3.subtract(P2,P1,v1);
	vec3.subtract(P4,P1,v2);
	
	vec3.normalize(v1,v1);
	vec3.normalize(v2,v2);
	
	// create plane
	vec3.cross(v1,v2,normal);
	distance = P1[0]*normal[0] + P1[1]*normal[1] + P1[2] * normal[2];
	plane.Set(normal[0], normal[1], normal[2], distance);
	
	// plane is behind the ray
	var negativeOrigin = vec3.create(this.origin);
	negativeOrigin[0] = -negativeOrigin[0];
	negativeOrigin[1] = -negativeOrigin[1];
	negativeOrigin[2] = -negativeOrigin[2];
	var distanceToPlane = plane.Distance(negativeOrigin);
	if (distanceToPlane < 0)
	{
		return -1;
	}
	
	// find intersect point
	//var intersectZ = 0;
	var angle = vec3.dot(this.direction, plane.normal);
	if (angle <= 0)
	{
		return -1;
	}
	
	vec3.scale(this.direction,distanceToPlane/angle,intersect);
	//intersectZ = intersect[2];
	vec3.add( this.origin, intersect, intersect );
	//intersect[2] = intersectZ;
	
	// find if the intersect is in the square
	vec3.subtract(P4, P3, v3); 			// P4 - P3
	vec3.subtract(intersect, P1, v4); 	// I - P1
	vec3.subtract(intersect, P3, v5); 	// I - P3
	
	// normalize
	vec3.normalize(v3,v3);
	vec3.normalize(v4,v4);
	vec3.normalize(v5,v5);
	
	// V1 dot V4 and V3 dot V5 ( checks X axis )
	var angle1 = vec3.dot(v1,v4);
	var angle2 = vec3.dot(v3,v5);
	
	// some added stuff ( checks y axis)
	var v6 = vec3.create(); // P2 - P3
	vec3.subtract(P2,P3,v6);
	vec3.normalize(v6,v6);
	
	var angle3 = vec3.dot(v2,v4);
	var angle4 = vec3.dot(v5,v6);
	
	// check if ray intersect is inside rectangle
	if (angle1 > 0 && angle2 > 0 && angle3 > 0 && angle4 > 0)
	{
		return DistanceVec3(intersect, this.origin);
	}
	
	// the ray is outside the rectangle
	return -1;
};

Ray.prototype.IntersectsBox = function(box)
{
	var NTR	= vec3.create(box.ntr);
	var NBR = vec3.create(box.nbr);
	var NBL = vec3.create(box.nbl);
	var NTL = vec3.create(box.ntl);
	
	var FTR	= vec3.create(box.ftr);
	var FBR = vec3.create(box.fbr);
	var FBL = vec3.create(box.fbl);
	var FTL = vec3.create(box.ftl);
	
	// check front
	var front = this.IntersectsPlane(FTR, FBR, FBL, FTL);
	
	// // check back
	var back = this.IntersectsPlane(NTL, NBL, NBR, NTR);
	
	// // check left
	var left = this.IntersectsPlane(FTL, FBL, NBL, NTL);
	
	// // check right
	var right = this.IntersectsPlane(NTR, NBR, FBR, FTR);
	
	// // check top
	var top = this.IntersectsPlane(FTL, NTL, NTR, FTR);
	
	// // check bottom
	var bottom = this.IntersectsPlane(FBR, NBR, NBL, FBL);
	
	if (front > 0 || back > 0 || left > 0 || right > 0 || top > 0 || bottom > 0)
	{
		var distance = Infinity;
		if (front >= 0)
		{
			distance = min (distance, front);
		}
		if (back >= 0)
		{
			distance = min (distance, back);
		}
		if (left >= 0)
		{
			distance = min (distance, left);
		}
		if (right >= 0)
		{
			distance = min (distance, right);
		}
		if (top >= 0)
		{
			distance = min (distance, top);
		}
		if (bottom >= 0)
		{
			distance = min (distance, bottom);
		}
		
		return distance;
	}
	
	return -1; // does not intersect
};

////////////////////////////
// Boolean Collision
////////////////////////////
function Colliding(a, b)
{
	var boxA = a.mesh.boundingBox;
	var boxB = b.mesh.boundingBox;
	
	/*
	/////////////////////////
	PA = coordinate position of the center of A
	Ax = unit vector representing the x-axis of A
	Ay = unit vector representing the y-axis of A
	Az = unit vector representing the z-axis of A
	WA = half width of A (corresponds with the local x-axis of A)
	HA = half height of A (corresponds with the local y-axis of A)
	DA = half depth of A (corresponds with the local z-axis of A)
	
	PB = coordinate position of the center of B
	Bx = unit vector representing the x-axis of B
	By = unit vector representing the y-axis of B
	Bz = unit vector representing the z-axis of B
	WB = half width of B (corresponds with the local x-axis of B)
	HB = half height of B (corresponds with the local y-axis of B)
	DB = half depth of B (corresponds with the local z-axis of B)
	
	T = PB � PA
	Rij = Ai �Bj
	
	////////////////////////
	*/
	
	var Wa = boxA.halfW;
	var Ha = boxA.halfH;
	var Da = boxA.halfD;
	
	var Wb = boxB.halfW;
	var Hb = boxB.halfH;
	var Db = boxB.halfD;
	
	var Rxx = Math.abs(vec3.dot(boxA.normalX, boxB.normalX)) + 0.001;
	var Rxy = Math.abs(vec3.dot(boxA.normalX, boxB.normalY)) + 0.001;
	var Rxz = Math.abs(vec3.dot(boxA.normalX, boxB.normalZ)) + 0.001;
	var Ryx = Math.abs(vec3.dot(boxA.normalY, boxB.normalX)) + 0.001;
	var Ryy = Math.abs(vec3.dot(boxA.normalY, boxB.normalY)) + 0.001;
	var Ryz = Math.abs(vec3.dot(boxA.normalY, boxB.normalZ)) + 0.001;
	var Rzx = Math.abs(vec3.dot(boxA.normalZ, boxB.normalX)) + 0.001;
	var Rzy = Math.abs(vec3.dot(boxA.normalZ, boxB.normalY)) + 0.001;
	var Rzz = Math.abs(vec3.dot(boxA.normalZ, boxB.normalZ)) + 0.001;
	
	var T = vec3.create();
	var centerA = vec3.create();//boxA.center;
	var centerB = vec3.create();//boxB.center;
	
	vec3.add(centerA, a.position, centerA);
	vec3.add(centerB, b.position, centerB);
	
	// vec3.add(boxA.center, a.position, centerA);
	// vec3.add(boxB.center, b.position, centerB);
	
	vec3.subtract(centerB, centerA, T);
	
	if ( Math.abs(vec3.dot(T, boxA.normalX)) > Wa + Math.abs(Wb * Rxx) + Math.abs(Hb * Rxy) + Math.abs(Db * Rxz)) return false;
	if ( Math.abs(vec3.dot(T, boxA.normalY)) > Ha + Math.abs(Wb * Ryx) + Math.abs(Hb * Ryy) + Math.abs(Db * Ryz)) return false;
	if ( Math.abs(vec3.dot(T, boxA.normalZ)) > Da + Math.abs(Wb * Rzx) + Math.abs(Hb * Rzy) + Math.abs(Db * Rzz)) return false;                                          
	if ( Math.abs(vec3.dot(T, boxB.normalX)) > Wb + Math.abs(Wa * Rxx) + Math.abs(Ha * Ryx) + Math.abs(Da * Rzx)) return false;
	if ( Math.abs(vec3.dot(T, boxB.normalY)) > Hb + Math.abs(Wa * Rxy) + Math.abs(Ha * Ryy) + Math.abs(Da * Rzy)) return false;
	if ( Math.abs(vec3.dot(T, boxB.normalZ)) > Db + Math.abs(Wa * Rxz) + Math.abs(Ha * Ryz) + Math.abs(Da * Rzz)) return false;
	
	var cross = vec3.create();
	
	vec3.cross(boxA.normalX, boxB.normalX, cross);
	if ( Math.abs(vec3.dot(T, cross)) > Math.abs(Ha * Rzx) + Math.abs(Da * Ryx) + Math.abs(Hb * Rxz) + Math.abs(Db * Rxy)) return false;
	
	vec3.cross(boxA.normalX, boxB.normalY, cross);
	if ( Math.abs(vec3.dot(T, cross)) > Math.abs(Ha * Rzy) + Math.abs(Da * Ryy) + Math.abs(Wb * Rxz) + Math.abs(Db * Rxx)) return false;
	
	vec3.cross(boxA.normalX, boxB.normalZ, cross);
	if ( Math.abs(vec3.dot(T, cross)) > Math.abs(Ha * Rzz) + Math.abs(Da * Ryz) + Math.abs(Wb * Rxy) + Math.abs(Hb * Rxx)) return false;
	
	vec3.cross(boxA.normalY, boxB.normalX, cross);
	if ( Math.abs(vec3.dot(T, cross)) > Math.abs(Wa * Rzx) + Math.abs(Da * Rxx) + Math.abs(Hb * Ryz) + Math.abs(Db * Ryy)) return false;
	
	vec3.cross(boxA.normalY, boxB.normalY, cross);
	if ( Math.abs(vec3.dot(T, cross)) > Math.abs(Wa * Rzy) + Math.abs(Da * Rxy) + Math.abs(Wb * Ryz) + Math.abs(Db * Ryx)) return false;
	
	vec3.cross(boxA.normalY, boxB.normalZ, cross);
	if ( Math.abs(vec3.dot(T, cross)) > Math.abs(Wa * Rzz) + Math.abs(Da * Rxz) + Math.abs(Wb * Ryy) + Math.abs(Hb * Ryx)) return false;
	
	vec3.cross(boxA.normalZ, boxB.normalX, cross);
	if ( Math.abs(vec3.dot(T, cross)) > Math.abs(Wa * Ryx) + Math.abs(Ha * Rxx) + Math.abs(Hb * Rzz) + Math.abs(Db * Rzy)) return false;
	
	vec3.cross(boxA.normalZ, boxB.normalY, cross);
	if ( Math.abs(vec3.dot(T, cross)) > Math.abs(Wa * Ryy) + Math.abs(Ha * Rxy) + Math.abs(Wb * Rzz) + Math.abs(Db * Rzx)) return false;
	
	vec3.cross(boxA.normalZ, boxB.normalZ, cross);
	if ( Math.abs(vec3.dot(T, cross)) > Math.abs(Wa * Ryz) + Math.abs(Ha * Rxz) + Math.abs(Wb * Rzy) + Math.abs(Hb * Rzx)) return false;
	
	return true;
	
}