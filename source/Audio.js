/**
 * Stores all audio files and volume
 * has ability to play audio files
 */
function AudioHandler()
{
	this.audioSource 	= [];
	this.soundInstances = [];
	this.music			= [];
	
	this.masterVolume 	= 1;
	this.sfxVolume 		= 1;
	this.musicVolume 	= 1;
	
	this.maxAudioChannlels = 10;
	
	// prepare audio channels
	this.audioChannels = new Array();
	for ( i = 0; i < this.maxAudioChannlels; ++i) {
		this.audioChannels[i] 				= new Array();
		this.audioChannels[i]['channel'] 	= new Audio();
		this.audioChannels[i]['finished'] 	= -1;
	}
}

/**
 * Loads SFX to an array and places it into [name]
 * programmer is responsible to enter valid name
 * @param {Object} path
 * @param {Object} name
 * @return {Object} a loaded sound file for chaining
 */
AudioHandler.prototype.Load = function(path, name)
{
	var sndBuffer = new Audio(path);
	this.audioSource[name] = sndBuffer;
	return this.audioSource[name];
};

/**
 * Plays SFX selected by name, can only have [maxAudioChannlers] instance of each sound
 * @param {Object} name
 * @param {Object} volume
 * volume is relative to current master volume and sfx volume
 */
AudioHandler.prototype.Play = function (name) {
	for ( i = 0; i < this.audioChannels.length; ++i) {
			var thistime = new Date();
			if (this.audioChannels[i]['finished'] < thistime.getTime()) {
			this.audioChannels[i]['finished'] = thistime.getTime() + this.audioSource[name].duration*1000;
			
			this.audioChannels[i]['channel'].src = this.audioSource[name].src;
			this.audioChannels[i]['channel'].load();
			this.audioChannels[i]['channel'].play();
			break;
		}
	}
};

/**
 * Plays SFX selected by name, can only have one instance of each sound
 * @param {Object} name
 * @param {Object} volume
 * volume is relative to current master volume and sfx volume
 */
/*
AudioHandler.prototype.Play = function(name, volume)
{
	if (this.audioSource[name] !== 'undefined')
	{
		var newSound = this.audioSource[name];
		newSound.currentTime = 0;
		newSound.play();
		newSound.volume = this.masterVolume * this.sfxVolume * volume;
		this.soundInstances.push(newSound);
	}
};*/

/**
 * Clears ended sfx sounds
 */
AudioHandler.prototype.Clean = function()
{
	for (i = 0; i < this.soundInstances.length; ++i)
	{
		if (this.soundInstances[i].ended === true)
		{
			this.soundInstances.splice(i,1);
			--i;
		}
	}
};

/**
 * Loads music to an array and places into [name]
 * Programmer is responsible to enter valid name
 * @param {Object} path
 * @param {Object} name
 * @return {Object} a loaded music file for chaining
 */
AudioHandler.prototype.LoadMusic = function(path, name)
{
	var sndBuffer = new Audio(path);
	this.music[name] = sndBuffer;
	this.music[name].buffer = true;
	return this.music[name];
};

/**
 * Plays music by name
 * @param {Object} name
 */
AudioHandler.prototype.PlayMusic = function(name)
{
	this.music[name].volume = this.masterVolume * this.musicVolume;
	this.music[name].play();
};

/**
 * Pases the music
 * @param {Object} name
 */
AudioHandler.prototype.PauseMusic = function(name)
{
	this.music[name].pause();
};

/**
 * Pauses and resets the music
 * @param {Object} name
 */
AudioHandler.prototype.StopMusic = function(name)
{
	this.music[name].currentTime=0;
	this.music[name].pause();
};

