//object container
var objects = []; // all objects are stored here
var hudObjects = [];
var camera = new Camera();
var hudcamera = new Camera();
var rayPrev = new RayHit();

//rotate objects
var lastTime = new Date().getTime();
var cameraSpeed = 5;
//var cameraTurnRate = 1.3;
var cameraTurnRate = 0.1;
var elapsed = 0;
var app_timer = 0;

function animate() {
	if (lastTime != 0) {
		for (var i = 0; i < objects.length; ++i) {
			objects[i].rotation[1] += 10 * elapsed;
			objects[i].rotation[0] += 10 * elapsed;
		}
	}
}

//draw state
var fontstyle;
var myShader;
var myShaderEffect;
var myShaderEffect2;

function handleInput() {
	if (lastTime != 0) {
		// 'W' 87
		if (87 in engine.Input.KeysUp) {
			camera.moveForward(cameraSpeed*elapsed);
		}
		// 'A' 65
		if (65 in engine.Input.KeysUp) {
			camera.moveRight(-cameraSpeed*elapsed);
		}
		// 'S' 83
		if (83 in engine.Input.KeysUp) {
			camera.moveForward(-cameraSpeed*elapsed);
		}
		// 'D' 68
		if (68 in engine.Input.KeysUp) {
			camera.moveRight(cameraSpeed*elapsed);
		}
		
		if (engine.Input.MouseWheelDelta)
		{
			camera.moveForward(engine.Input.MouseWheelDelta*cameraSpeed*elapsed);
		}
		
		//arrow up
		if (38 in engine.Input.KeysUp) {
			camera.rotate(0,10*(cameraTurnRate*elapsed), 0);
		}
		//arrow left
		if (37 in engine.Input.KeysUp) {
			camera.rotate(10*(-cameraTurnRate*elapsed), 0, 0);
		}
		//arrow right
		if (39 in engine.Input.KeysUp)	{
			camera.rotate(10*(cameraTurnRate*elapsed), 0, 0);
		}
		//arrow down
		if (40 in engine.Input.KeysUp) {
			camera.rotate(0,10*(-cameraTurnRate*elapsed), 0);
		}
		
		//camera.rotate(-cameraTurnRate * elapsed * engine.Input.DeltaMouse.X, 0, 0);
		//camera.rotate(0, -cameraTurnRate * elapsed * engine.Input.DeltaMouse.Y, 0);

		//space
		if (32 in engine.Input.KeysUp) {
			camera.moveUp(cameraSpeed*elapsed);
			
		}
		//'X' 88
		if (88 in engine.Input.KeysUp) {
			camera.moveUp(-cameraSpeed*elapsed);
		}
		//'R' 82
		if (82 in engine.Input.KeysUp) {
			camera.setRotation(0,0,0);
			camera.setPosition(0,0,0);
		}
		//'L' 76
		if (76 in engine.Input.KeysUp) {
			fixedLight = false;
		}
		//'O' 79
		if (79 in engine.Input.KeysUp) {
			fixedLight = true;
		}
		
		/*
		 * For people who wonder why i have 0.0001 offset
		 * there is abug in camera that flips X when camera pitch is either 90 or -90
		 */
		
		//up cap
		if (camera.rotation[1] >= Camera.prototype.PI/2)
		{
			camera.rotation[1] = Camera.prototype.PI/2 - 0.0001;
		}
		
		//down cap
		if (camera.rotation[1] <= -Camera.prototype.PI/2)
		{
			camera.rotation[1] = -Camera.prototype.PI/2 + 0.0001; 
		}
		
		// mouse input test
		if (engine.Input.MouseDBCLK)
		{
			engine.Audio.Play("jump", 1);
		}
		if (engine.Input.MouseLBTN)
		{
			hudObjects[0].setColor(1,0,0);
		}
		else if (engine.Input.MouseMBTN)
		{
			hudObjects[0].setColor(0,1,0);
		}
		else if (engine.Input.MouseRBTN)
		{
			hudObjects[0].setColor(0,0,1);
		}
		else
		{
			hudObjects[0].setColor(1,1,1);
		}
	}
}

function initialize() {	
	
	// load shaders
	myShader = new PPLitShader();
	myShader.Load();
	myShaderEffect = new GridBackgroundShader();
	myShaderEffect.Load();
	myShaderEffect2 = new UnlitShader();
	myShaderEffect2.Load();
	
	// make font style
	fontstyle = new FontStyle();
	fontstyle.size = 40;
	fontstyle.fontFamily = "monospace";
	fontstyle.textAlign = "center";
	fontstyle.textBaseline = "middle";
	fontstyle.color = "#000";
	fontstyle.backgroundColor = "#FFF";
	fontstyle.useAlpha = false;
	
	// load audio
	engine.Audio.Load("audio/Hyperspace.wav", "jump");
	//var music = engine.Audio.LoadMusic("audio/testMusic.ogg", "music");
	//music.loop = true;
	//music.play();
	
	camera.CreatePerspectiveProjection(45, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0);
	hudcamera.CreateOrthographicProjection(0,gl.viewportWidth,0,gl.viewportHeight,0.1,1000);
	
	// teapot
	var teapot = new Object;
	var tempMesh = new Mesh;
	
	loadObj(gl, "models/cube.obj", tempMesh);

	teapot.mesh = tempMesh;
	teapot.setPosition(0.6,0.0,-9);
	teapot.setRotation(0,-20,0);
	teapot.setScale(2,1,1);
	teapot.color = [1,1,1];
	teapot.effect = myShader;
	
	objects.push(teapot);
	
	
	// torus
	var torus = new Object;
	var tempMesh = new Mesh;

	loadObj(gl, "models/cube.obj", tempMesh);

	torus.mesh = tempMesh;
	torus.setPosition(0.35,0.0,-11);
	torus.setRotation(0,-45,0);
	torus.setScale(1,5,1);
	torus.color = [1,1,1];
	torus.effect = myShader;
	
	objects.push(torus);
	
	// sphere
	var sphere = new Object;
	var tempMesh = new Mesh;

	loadObj(gl, "models/cube.obj", tempMesh);

	sphere.mesh = tempMesh;
	sphere.setPosition(1.1,0,-10);
	sphere.setRotation(0,0,0);
	sphere.setScale(1,1,1);
	sphere.color = [1,1,1];
	sphere.effect = myShader;
	
	objects.push(sphere);
	
	//box
	var cube = new Object;
	var tempMesh = new Mesh;

	loadObj(gl, "models/quad.obj", tempMesh);
	tempMesh.texture = engine.Graphics.CreateText("Hello Hello", fontstyle);//engine.Graphics.LoadTexture("models/cube.jpg");
	tempMesh.isTextured = true;
	cube.mesh = tempMesh;
	cube.setPosition(0,0,-40);
	cube.setRotation(0,0,0);
	cube.setScale(800,100,1);
	cube.color = [1,1,1];
	
	cube.effect = myShaderEffect2;
	
	hudObjects.push(cube);
}


function update() {
	var timeNow = new Date().getTime();
	elapsed = (timeNow - lastTime)/1000;
	app_timer += elapsed;
	
	if (elapsed < 1/30)
	{
		lastTime = timeNow;
		
		if (engine.Settings.ViewportChanged)
		{
			camera.CreatePerspectiveProjection(45, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0);
			engine.Settings.ViewportChanged = false;
		}
		
		handleInput();
		draw();
		animate();
	}
	lastTime = timeNow;
}

var debug = null;

function draw() 
{
	engine.Graphics.RenderToScreen();
	engine.Graphics.glClear();
	
	//myShaderEffect.Draw(app_timer);
	//gl.clear(gl.DEPTH_BUFFER_BIT);
	
	for (var i = 0; i < objects.length; ++i)
		objects[i].setColor(1,1,1);
	
	for (var i = 0; i < objects.length; ++i)
	{
		for (var k = i+1; k < objects.length; ++k)
		{
			if (Colliding(objects[i], objects[k]) === true)
			{
				objects[i].setColor(1,0,0);
				objects[k].setColor(1,0,0);
			}
		}
	}
	
	// ray test
	// if (rayPrev.object)
		// rayPrev.object.setColor(1,1,1);
	rayPrev = Ray.prototype.PickRay(camera, objects);
	if ( rayPrev.object && rayPrev.distance >= 0 )
		rayPrev.object.setColor(0,1,0);
	
	
	engine.Graphics.Draw(camera, true, objects);
	engine.Graphics.Draw(hudcamera, false, hudObjects);
}