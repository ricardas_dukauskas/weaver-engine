/**
 * This object captures and holds all user input
 * @param {Object} c is HTML5 canvas
 */
function InputHandler(/*canvas*/ c)
{
	//Parameters
	this.KeysUp = [];
	this.LastKey;
	this.MousePosition = new Vector2(0,0);
	this.DeltaMouse = new Vector2(0,0);
	this.MouseLBTN;
	this.MouseRBTN;
	this.MouseWheelDelta;
	this.MouseClick;
	this.MouseDBCLK;
	
	// private
	this.canvas = c;
	this.isFF; // is firefox
	
	//register event listeners
	document.addEventListener("keydown", bind(this, this.KeyDown), false);
	document.addEventListener("keyup", bind(this, this.KeyUp), false);
	
	document.addEventListener("mousemove", bind(this, this.MouseMove), false);
	document.addEventListener("mousedown", bind(this, this.MouseDown), false);
	document.addEventListener("mouseup", bind(this, this.MouseUp), true);
	document.addEventListener('dblclick', bind(this, this.MouseDBClick), false);
	
	//FF doesn't recognize mousewheel as of FF3.x
	var mousewheelevt;
	if(/Firefox/i.test(navigator.userAgent))
	{
		mousewheelevt = "DOMMouseScroll";
		this.isFF = true;
	}
	else
	{
		mousewheelevt = "mousewheel";
		this.isFF = false;
	}
	document.addEventListener(mousewheelevt, bind(this, this.MouseWheel), false);
	
	// touch is not implemented yet
	// document.addEventListener("touchstart", , false); 
	// document.addEventListener("touchend", , false);
	// document.addEventListener("touchmove", , false);
	
	document.addEventListener('pointerlockchange', bind(this, this.PointerLockChange), false);
	document.addEventListener('mozpointerlockchange', bind(this, this.PointerLockChange), false);
	document.addEventListener('webkitpointerlockchange', bind(this, this.PointerLockChange), false);
	
	document.addEventListener('pointerlockerror', bind(this, this.PointerLockError), false);
	document.addEventListener('mozpointerlockerror', bind(this, this.PointerLockError), false);
	document.addEventListener('webkitpointerlockerror', bind(this, this.PointerLockError), false);
}

/**
 * Resets some delta variebles back to 0
 * since most browsers don't bother doing that
 */
InputHandler.prototype.Restore = function()
{
	this.DeltaMouse.X = 0;
	this.DeltaMouse.Y = 0;
	this.MouseWheelDelta = 0;
	
	this.MouseClick = false;
	this.MouseDBCLK = false;
};

/**
 * Stores pressed key in an array KeysUp and LastKey
 * @param {Object} event
 */
InputHandler.prototype.KeyDown = function(event) 
{
	this.LastKey = event.keyCode;
	this.KeysUp[event.keyCode] = true;
	switch(event.keyCode){
		case 37: case 39: case 38:  case 40: // Arrow keys
		case 32: case 17: event.preventDefault(); break; // Space / L-Ctrl
		case 116: event.preventDefault(); engine.Settings.ToggleFullscreen(); break;
		default: break; // do not block other keys
	}
};

/**
 * Deletes released keys from the array KeysUp
 * @param {Object} event
 */
InputHandler.prototype.KeyUp = function(event) {
	delete this.KeysUp[event.keyCode];
};

/**
 * Captures mouse movement and mouse position
 * @param {Object} event
 */
InputHandler.prototype.MouseMove = function(event)
{
	this.DeltaMouse.X =	  event.movementX   	||
		                  event.mozMovementX    ||
		                  event.webkitMovementX ||
		                  0,
    this.DeltaMouse.Y =   event.movementY       ||
		                  event.mozMovementY    ||
		                  event.webkitMovementY ||
		                  0;
						  
	if ( this.canvas === document ) {
		this.MousePosition.X = event.pageX;
		this.MousePosition.Y = event.pageY;
	} else {
		if (engine.Settings.Fullscreen === true)
		{
			this.MousePosition.X = event.pageX;
			this.MousePosition.Y = event.pageY;
		}
		else
		{
			this.MousePosition.X = event.pageX - this.canvas.offsetLeft;
			this.MousePosition.Y = event.pageY - this.canvas.offsetTop;
		}
	}
};

/**
 * Locks mouse cursor, also hides it. DeltaMouse is still updated correctly.
 */
InputHandler.prototype.LockPointer = function() {
	canvas.requestPointerLock = this.canvas.requestPointerLock    ||
								this.canvas.mozRequestPointerLock ||
								this.canvas.webkitRequestPointerLock;
	canvas.requestPointerLock();
};

/**
 * Called on mouse lock/unlock
 */
InputHandler.prototype.PointerLockChange = function () {
  if (document.mozPointerLockElement === this.canvas ||
      document.webkitPointerLockElement === this.canvas) {
    console.log("Pointer Lock was successful.");
  } else {
    console.log("Pointer Lock was lost.");
  }
};

/**
 * called if pointer lock fails.
 */
InputHandler.prototype.PointerLockError = function () {
  console.log("Error while locking pointer.");
};

/**
 * Captures mouse click, supports Left, Middle and Right mouse buttons
 * @param {Object} event
 * sets pressed button to true
 */
InputHandler.prototype.MouseDown = function(event) {
	if (this.MousePosition.X > 0 && this.MousePosition.X < this.canvas.width && 
		this.MousePosition.Y > 0 && this.MousePosition.Y < this.canvas.height) {
		event.preventDefault();
	}
	
	var left = engine.IE ? 1 : 0;
	var middle = engine.IE ? 3 : 1;
	var right = 2;
	
	if (event.button === left)
	{
		this.MouseLBTN = true;
		this.MouseClick = true;
	}
	else if (event.button === right)
	{
		this.MouseRBTN = true;
	}
	else if (event.button === middle)
	{
		this.MouseMBTN = true;
	}
};

/**
 * Captures mouse button release, supports Left, Middle and Right mouse buttons
 * @param {Object} event
 * sets released button to false
 */
InputHandler.prototype.MouseUp = function(event) {
	var left = engine.IE ? 1 : 0;
	var middle = engine.IE ? 3 : 1;
	var right = 2;
	
	if (event.button === left)
	{
		this.MouseLBTN = false;
	}
	else if (event.button === right)
	{
		this.MouseRBTN = false;
	}
	else if (event.button === middle)
	{
		this.MouseMBTN = false;
	}
};

/**
 * Captures double click event
 * @param {Object} event
 */
InputHandler.prototype.MouseDBClick = function (event)
{
	this.MouseDBCLK = true;
};

/**
 * Captures mouse wheel data
 * @param {Object} event
 */
InputHandler.prototype.MouseWheel = function(event)
{
	event.preventDefault();
	this.MouseWheelDelta = this.isFF ? sign(event.detail) * (-120) : event.wheelDelta;
};

/*
//ADD TOUCH EVENT LISTENER
InputHandler.prototype.TouchStart = function(event){
	if(this.isMouseDown == true){
		isMouseDown = true;
	} else {
		isMouseDown = true;
		mouseX = e.touches[0].pageX;
		mouseY = e.touches[0].pageY;
		normalizeMouse(mouseX,mouseY);
		if (mouseX > 0 && mouseX < game.model.canvasWidth && mouseY > 0 && mouseY < game.model.canvasHeight){
			e.preventDefault();
		}
	}
};

InputHandler.prototype.TouchEnd = function(e){
	isMouseDown = false;
};

InputHandler.prototype.TouchMove = function(e){
	mouseX = e.touches[0].pageX ;
	mouseY = e.touches[0].pageY;
	
	isMouseDown = true;
	normalizeMouse(mouseX,mouseY);
	if (mouseX > 0 && mouseX < game.model.canvasWidth && mouseY > 0 && mouseY < game.model.canvasHeight){
		e.preventDefault();
	}
};*/