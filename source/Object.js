/**
 * Holds a mesh
 * model, texture and a varieble isTextured
 * isTextured is programmers responsability
 */
function Mesh() {
	this.model;
	this.texture;
	this.isTextured = false;
	this.boundingBox;
}

/**
 * Holds 
 * an array of models
 * object name
 * apply light (bool)
 * position vec3
 * rotation vec3
 * scale vec3
 * color vec3
 */
function Object() {
	this.mesh;
	this.name;
	this.useLight = true;
	this.position = vec3.create();
	this.rotation = vec3.create();
	this.scale = vec3.create([1,1,1]);	
	this.color = vec3.create([1,1,1]);
	this.localMat = mat4.create();
	this.effect;
	
	// some performence bools
	this.updateMat = true;
	this.updateBox = true;
}

Object.prototype.PI 	 = 3.141592653;
Object.prototype.TO_RADS = 0.017453293; // PI/180
Object.prototype.TO_DEGR = 57.2957795130; // 180/PI

/**
 * sets position of the object
 * @param {Object} x
 * @param {Object} y
 * @param {Object} z
 */
Object.prototype.setPosition = function(x,y,z) {
	this.position[0] = x;
	this.position[1] = y;
	this.position[2] = z;
	this.updateMat = true;
};

/**
 * sets rotation of the object
 * @param {Object} x
 * @param {Object} y
 * @param {Object} z
 */
Object.prototype.setRotation = function(x,y,z) {
	this.rotation[0] = x;
	this.rotation[1] = y;
	this.rotation[2] = z;
	this.updateMat = true;
};

/**
 * sets color of the object
 * @param {Object} r
 * @param {Object} g
 * @param {Object} b
 */
Object.prototype.setColor = function(r,g,b) {
	this.color[0] = r;
	this.color[1] = g;
	this.color[2] = b;
};

/**
 * sets scale of the object
 * @param {Object} x
 * @param {Object} y
 * @param {Object} z
 */
Object.prototype.setScale = function(x,y,z) {
	this.scale[0] = x;
	this.scale[1] = y;
	this.scale[2] = z;
	this.updateMat = true;
};

Object.prototype.ForceUpdate = function()
{
	this.updateMat = true;
	this.updateBox = true;
}

Object.prototype.UpdateTransform = function()
{
	if (this.updateMat)
	{	
		mat4.identity(this.localMat);
		mat4.translate(this.localMat, this.position);
		
		var rotationMatrix = mat4.create();
		mat4.identity(rotationMatrix);
		
		mat4.rotate(rotationMatrix, degToRad(this.rotation[0]), [1, 0, 0]);
		mat4.rotate(rotationMatrix, degToRad(this.rotation[1]), [0, 1, 0]);
		mat4.rotate(rotationMatrix, degToRad(this.rotation[2]), [0, 0, 1]);
		
		mat4.multiply(this.localMat, rotationMatrix, this.localMat);
		mat4.scale(this.localMat, this.scale, this.localMat);
		
		// // rotate normals
		// this.mesh.boundingBox.normalX = vec3.
		// this.mesh.boundingBox.normalY = vec3.
		// this.mesh.boundingBox.normalZ = vec3.
		
		this.mesh.boundingBox.halfW = this.mesh.boundingBox.original_halfW * this.scale[0];
		this.mesh.boundingBox.halfH = this.mesh.boundingBox.original_halfH * this.scale[1];
		this.mesh.boundingBox.halfD = this.mesh.boundingBox.original_halfD * this.scale[2];
		
		this.mesh.boundingBox.center[0] = this.mesh.boundingBox.halfW;
		this.mesh.boundingBox.center[1] = this.mesh.boundingBox.halfH;
		this.mesh.boundingBox.center[2] = this.mesh.boundingBox.halfD;
		
		var vec = vec3.create();

		vec[0] = 1;
		vec[1] = 0;
		vec[2] = 0;
		mat4.multiplyVec3(rotationMatrix, vec, this.mesh.boundingBox.normalX);

		vec[0] = 0;
		vec[1] = 1;
		vec[2] = 0;
		mat4.multiplyVec3(rotationMatrix, vec, this.mesh.boundingBox.normalY);

		vec[0] = 0;
		vec[1] = 0;
		vec[2] = 1;
		mat4.multiplyVec3(rotationMatrix, vec, this.mesh.boundingBox.normalZ);

		
		this.updateMat = false;
		this.updateBox = true;
	}
}

/**
 * Updates positions of model bounding boxes
 */
Object.prototype.UpdateBoundingBoxes = function() {
	this.UpdateTransform();
	
	if (this.updateBox)
	{
		for (k = 0; k < this.mesh.boundingBox.vertices.length; ++k)
		{
			mat4.multiplyVec3(this.localMat, this.mesh.boundingBox.originVertices[k], this.mesh.boundingBox.vertices[k]);
		}
		this.updateBox = false;
	}
};