/**
 * Holds all font data
 */
function FontStyle()
{
	this.size 			= 12;
	this.fontFamily 	= "monospace";
	this.textAlign 		= "left";
	this.textBaseline 	= "top";
	
	this.color 			= "#000";
	this.backgroundColor= "#FFF";
	this.useAlpha 		= false;
}

/**
 * Sets most of the font data to the current 2D context
 * does not set color values
 */
FontStyle.prototype.Activate = function()
{
	ctx.textAlign = this.textAlign;
	ctx.textBaseline = this.textBaseline;
	ctx.font = this.size + "px " + this.fontFamily;
};
