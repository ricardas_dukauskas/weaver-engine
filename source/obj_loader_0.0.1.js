/**
 * Holds information about bounding box
 */
function BoundingBox()
{	
	this.vertices = [];
	this.originVertices = [];
	this.radius;
	
	this.normalX = vec3.create();
	this.normalY = vec3.create();
	this.normalZ = vec3.create();
	
	this.center = vec3.create();
	
	this.original_halfW;
	this.original_halfH;
	this.original_halfD;
	
	this.halfW;
	this.halfH;
	this.halfD;
	
	this.ntl;
	this.ntr;
	this.nbl;
	this.nbr;
	this.ftl;
	this.ftr;
	this.fbl;
	this.fbr;
}

BoundingBox.prototype.PushVertex = function(v)
{
	this.vertices.push(v);
	var copy = vec3.create(v);
	this.originVertices.push(copy);
};

BoundingBox.prototype.Initialize = function()
{
	this.normalX[0] = 1;
	this.normalX[1] = 0;
	this.normalX[2] = 0;
	
	this.normalY[0] = 0;
	this.normalY[1] = 1;
	this.normalY[2] = 0;
	
	this.normalZ[0] = 0;
	this.normalZ[1] = 0;
	this.normalZ[2] = 1;
	
	this.halfW = (this.ntr[0] - this.ntl[0]) / 2;
	this.halfH = (this.ntl[1] - this.nbl[1]) / 2;
	this.halfD = (this.ftl[2] - this.ntl[2]) / 2;
	
	this.original_halfW = this.halfW;
	this.original_halfH = this.halfH;
	this.original_halfD = this.halfD;
	
	this.center[0] = this.halfW;
	this.center[1] = this.halfH;
	this.center[2] = this.halfD;
};

/**
 * a container for raw model data 
 */
function ModelData()
{
	this.vertices = [];
	this.indices = [];
	this.normals = [];
	this.vTexture = [];
}

/**
 * a model class, holds pointers to raw data 
 */
function Model()
{
	this.verticesPtr;
	this.indicesPtr;
	this.normalsPtr;
	this.vTexturePtr;
	//this.boundingBox = new BoundingBox(); // used for frustum culling
}

/**
 * vector3 data container
 * should be replaced with array 
 */
function ModelVec3()
{
	this.x = 0;
	this.y = 0;
	this.z = 0;
}

/**
 * loads obs file data from file
 * @param {Object} url - path to the model 
 */
function loadData(url)
{
	var file = null;
	var string;
	var stringLines;
	file = new XMLHttpRequest();
	file.open( "POST", url, false );
	file.send( null );
	
	//get raw data
	string = file.responseText;
	
	//split data into lines
	stringLines = string.split("\n");
	
	return stringLines;
}

/**
 * parses obj file and stores raw data into model object
 * @param {object} data - raw obj data
 * @param {Model} model - raw model data 
 */
function parseOBJ(/*raw data*/data, /*parsed obj model*/model, /*bounding box*/ box)
{
	var ind = 1; // index
	var length = data.length;

	var temp_vertices = []; //loaded vertex data
	var temp_normals = []; //loaded normal data
	var temp_texture = []; //loaded uv coordinates data

	var minX = Infinity;
	var minY = Infinity;
	var minZ = Infinity;
	var maxX = -Infinity;
	var maxY = -Infinity;
	var maxZ = -Infinity;
	
	//add origin point
	model.vertices.push(0);
	model.vertices.push(0);
	model.vertices.push(0);
	temp_vertices.push(0);
	temp_vertices.push(0);
	temp_vertices.push(0);

	model.normals.push(0);
	model.normals.push(0);
	model.normals.push(0);
	temp_normals.push(0);
	temp_normals.push(0);
	temp_normals.push(0);

	model.vTexture.push(0);
	model.vTexture.push(0);
	temp_texture.push(0);
	temp_texture.push(0);
	
	
	//while (data.length != 0)
	while (length != 0)
	{
		//readin line
		//var line = data[0];
		var line = data[0];
		var buffer = line.split(" ");
		
		data.splice(0,1);
		--length;
		
		
		if (buffer[0] == "v") //vertex
		{
			for (var i = 1; i <= 3; ++i)
			{
				temp_vertices.push(parseFloat(buffer[i]));
			}
			minX = min(minX,parseFloat(buffer[1]));
			maxX = max(maxX,parseFloat(buffer[1]));
			minY = min(minY,parseFloat(buffer[2]));
			maxY = max(maxY,parseFloat(buffer[2]));
			minZ = min(minZ,parseFloat(buffer[3]));
			maxZ = max(maxZ,parseFloat(buffer[3]));
		}
		else if (buffer[0] == "vt") //vertex texture
		{
			for (var i = 1; i <= 2; ++i)
			{
				temp_texture.push(parseFloat(buffer[i]));
			}
		}
		else if (buffer[0] == "vn") //vertex normal
		{
			for (var i = 1; i <= 3; ++i)
			{
				temp_normals.push(parseFloat(buffer[i]));
			}
		}
		else if (buffer[0] == "f") //face
		{
			//delete first element
			buffer.splice(0,1);
			for (var i = 0; i < 3; ++i)
			{
				var faceSplit = buffer[i].split("/");
				
				model.indices.push(ind);
				++ind;

				model.vertices.push(temp_vertices[parseInt(faceSplit[0]) * 3 + 0]); // - this is the x value
				model.vertices.push(temp_vertices[parseInt(faceSplit[0]) * 3 + 1]); // - this is the y value
				model.vertices.push(temp_vertices[parseInt(faceSplit[0]) * 3 + 2]); // - this is the z value


				if (faceSplit[1] != null)
				{
					model.vTexture.push(temp_texture[parseInt(faceSplit[1]) * 2 + 0]); // - this is the x value
					model.vTexture.push(temp_texture[parseInt(faceSplit[1]) * 2 + 1]); // - this is the y value (NOT inverted it apppears that you dont need to in WebGL)
				}


				model.normals.push(temp_normals[parseInt(faceSplit[2]) * 3 + 0]); // - this is the x value
				model.normals.push(temp_normals[parseInt(faceSplit[2]) * 3 + 1]); // - this is the y value
				model.normals.push(temp_normals[parseInt(faceSplit[2]) * 3 + 2]); // - this is the z value
			}
		}
	}//while end
	
	//well too bad now it will take way more to load
	//since we need to calculate normals on our own :(
	if (temp_normals.length == 3)
	{
		model.normals = model.vertices;
	
		//get face normals
		calculateNormals(model);
	}
	
	box.ntl = vec3.create([minX, maxY, minZ]);
	box.ntr = vec3.create([maxX, maxY, minZ]);
	box.nbl = vec3.create([minX, minY, minZ]);
	box.nbr = vec3.create([maxX, minY, minZ]);
	box.ftl = vec3.create([minX, maxY, maxZ]);
	box.ftr = vec3.create([maxX, maxY, maxZ]);
	box.fbl = vec3.create([minX, minY, maxZ]);
	box.fbr = vec3.create([maxX, minY, maxZ]);

	box.PushVertex(box.ntl);
	box.PushVertex(box.ntr);
	box.PushVertex(box.nbl);
	box.PushVertex(box.nbr);
	box.PushVertex(box.ftl);
	box.PushVertex(box.ftr);
	box.PushVertex(box.fbl);
	box.PushVertex(box.fbr);
	
	// box.PushVertex(vec3.create([minX, maxY, minZ]));
	// box.PushVertex(vec3.create([maxX, maxY, minZ]));
	// box.PushVertex(vec3.create([minX, minY, minZ]));
	// box.PushVertex(vec3.create([maxX, minY, minZ]));
	// box.PushVertex(vec3.create([minX, maxY, maxZ]));
	// box.PushVertex(vec3.create([maxX, maxY, maxZ]));
	// box.PushVertex(vec3.create([minX, minY, maxZ]));
	// box.PushVertex(vec3.create([maxX, minY, maxZ]));
	
	var maxValue = max( max(maxX, maxY), maxZ);
	var minValue = min( min(minX, minY), minZ);
	box.radius = max(Math.abs(minValue), maxValue);
	
	box.Initialize();
}

/**
 * calculates normals, flat shaded 
 * @param {Object} model
 */
function calculateNormals(model)
{
	var a = new ModelVec3;
	var b = new ModelVec3;
	var c = new ModelVec3;
	var n = new ModelVec3;
	
	for (var i = 0; i < model.indices.length; i+=3)
	{
		var index;

		index = model.indices[i]*3;
		a.x = model.vertices[index];
		a.y = model.vertices[index+1];
		a.z = model.vertices[index+2];
		
		index = model.indices[i+1]*3;
		b.x = model.vertices[index];
		b.y = model.vertices[index+1];
		b.z = model.vertices[index+2];
		
		index = model.indices[i+2]*3;
		c.x = model.vertices[index];
		c.y = model.vertices[index+1];
		c.z = model.vertices[index+2];

		n = calculateNormal(a,b,c);

		for (var k = 0; k < 3; ++k)
		{
			index = model.indices[i+k]*3;
			model.normals[index] = n.x;
			model.normals[index+1] = n.y;
			model.normals[index+2] = n.z;
		}
	}
}

/**
 * calculates a normal vector from 3 points 
 * @param {Object} pointA
 * @param {Object} pointB
 * @param {Object} pointC
 * @return n - normal
 */
function calculateNormal(pointA, pointB, pointC)
{
	var a = new ModelVec3;
	var b = new ModelVec3;
	var n = new ModelVec3;

	a.x = pointB.x - pointA.x;
	a.y = pointB.y - pointA.y;
	a.z = pointB.z - pointA.z;
	
	b.x = pointC.x - pointA.x;
	b.y = pointC.y - pointA.y;
	b.z = pointC.z - pointA.z;

	//get cross porduct
	n.x = (a.y * b.z) - (a.z * b.y);
	n.y = (a.z * b.x) - (a.x * b.z);
	n.z = (a.x * b.y) - (a.y * b.x);

	//normalize
	var length = Math.sqrt(n.x*n.x + n.y*n.y + n.z*n.z);

	if (length == 0)
	{
		n.x = 0;  
		n.y = 0;
		n.z = 0;
		
		return n;
	}
	
	n.x = n.x / length;  
	n.y = n.y / length;
	n.z = n.z / length;

	return n;
}

//buffers
var cubeVertexBuffer;
var cubeTextureBuffer;
var cubeVertexNormalBuffer;
var cubeIndexBuffer;
var cube = new Model();

/**
 * loads model to GPU 
 * @param {Object} gl - webgl context
 * @param {Object} data - raw model data
 * @param {Object} box - bounding box
 */
function loadToGPU(gl, data, box) {
	
	var model = new Model();
	
	//setColor(cube,1.0,0.5,1.0);
	
	//vertices
	model.verticesPtr = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, model.verticesPtr);
	
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.vertices), gl.STATIC_DRAW);
	model.verticesPtr.itemSize = 3;
	model.verticesPtr.numItems = data.vertices.length / 3;
	
	//// COLOR STUFF
	//cubeVertexColorBuffer = gl.createBuffer();
	//gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexColorBuffer);
    //
	//gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.color), gl.STATIC_DRAW);
	//cubeVertexColorBuffer.itemSize = 3;
	//cubeVertexColorBuffer.numItems = data.color.length;

	//uv coordinates
	model.vTexturePtr = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, model.vTexturePtr);
	
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.vTexture), gl.STATIC_DRAW);
	model.vTexturePtr.itemSize = 2;
	model.vTexturePtr.numItems = data.vTexture.length/2;
	
	//normals
	model.normalsPtr = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, model.normalsPtr);

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.normals), gl.STATIC_DRAW);
	model.normalsPtr.itemSize = 3;
	model.normalsPtr.numItems = data.normals.length / 3;
	
	//indices
	model.indicesPtr = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.indicesPtr);
	
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(data.indices), gl.STATIC_DRAW);
	model.indicesPtr.itemSize = 1;
	model.indicesPtr.numItems = data.indices.length;
	
	model.boundingBox = box;
	
	return model;
}

/**
 * loads OBJ model to GPU 
 * @param {Object} gl - webgl context
 * @param {Object} path - url path
 * @return {Model} model object
 */
function loadObj(gl, path, mesh)
{
	var data = new ModelData();
	var box = new BoundingBox();
	parseOBJ(loadData(path), data, box);
	
	mesh.model = loadToGPU(gl, data, box);
	mesh.boundingBox = box;
	return mesh.model;
}